<?php

function isActiveUri($uri, $output = 'active')
{
	if( is_array($uri) ) {
		foreach ($uri as $u) {
			if (Request::is($u)) {
				return $output;
			}
		}
	} else {
		if (Request::is($uri)){
			return $output;
		}
	}
}

function moneyFormatter($value = 0)
{
	return number_format($value, 0, ',', '.');
}