<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;
use Validator;
use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }
    
    public function login(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
            // 'g-recaptcha-response' => 'required|captcha',
        ]);

        if ($validator->fails()) {
            return redirect('login')
                ->withInput()
                ->withErrors($validator);
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1, 'deleted_at' => null])) {
            $user =  User::findOrFail(Auth::user()->id);
            $user->update([
                'last_login' => Carbon::now('Asia/Jakarta')
            ]);
            return redirect()->intended('/home');
        }

        return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
            'approve' => 'Password Anda salah',
        ]);
    }
}
