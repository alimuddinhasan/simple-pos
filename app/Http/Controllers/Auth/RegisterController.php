<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;

use App\Models\Merchant\Merchant;
use App\User;
use App\Role;
use Gerardojbaez\Laraplans\Models\Plan;

class RegisterController extends Controller
{
    use VerifiesUsers;
    use RegistersUsers;

    public function register(Request $request)
    {
        $input = $request->all();
        $merchant = Merchant::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'active' => true
        ]);
    
        $user = User::create([
            'name' => $input['email'],
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
            'merchant_id' => $merchant->id,
            'active' => true
        ]);
        $user->roles()->attach(Role::where('name', 'merchant-admin')->first());
        UserVerification::generate($user);
        UserVerification::send($user, 'Verifikasi Akun Anda', 'noreply@bakulkasir.com', 'Support BakulKasir.com');
    
        $merchant->newSubscription('main', Plan::find(1))->create();
        // return 
    }
}
