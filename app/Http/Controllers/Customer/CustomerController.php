<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer\Customer;
use GuzzleHttp\Client;

use Datatables;
use Auth;

class CustomerController extends Controller
{
  private $title = 'Pelanggan';

  public function index(Request $request)
  {
    $data['_title'] = $this->title;
    return view('customers.index', compact('data'));
  }

  public function fetchAll()
  {
    $query = Customer::where([
      ['deleted_at', null],
      ['merchant_id', Auth::user()->merchant_id]
    ]);
    return Datatables::of($query)
      // ->editColumn('active', function ($data) {
      //   return $data->active ? '<span class="label label-success">Available</span>' : '<span class="label label-danger">Not Available</span>';
      // })
      ->addColumn('action', function ($data) {
        $html = '';
        $html .= ' <a href="'.url('customer/'.$data->id.'/detail').'" class="btn btn-xs btn-default"><i class="fa fa-search"></i></a>';
        $html .= ' <a href="'.url('customer/'.$data->id.'/edit').'" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>';
        $html .= ' <a href="'.url('customer/'.$data->id.'/delete').'" class="btn btn-xs btn-danger delete-btn"><i class="fa fa-trash"></i></a>';
        return $html;
      })
      ->rawColumns(['action'])->make(true);
  }

  public function find (Request $request)
  {
    $q = $request->q;

    $query = Customer::where([
      ['deleted_at', null],
      ['active', true],
      ['merchant_id', Auth::user()->merchant_id],
      ['name', 'like', '%'.$q.'%']
    ])->take(10)->get();
    
    return $query;
  }

  public function get ($id)
  {
    $query = Customer::findOrFail($id);
    return $query;
  }

  public function showForm(Request $request, $id = null)
  {
    $data['_title'] = $this->title;
    if ($id) {
      $data['customer'] = Customer::findOrFail($id);
      return view('customers.form', compact('data'));
    } else {
      return view('customers.form', compact('data'));
    }
  }

  public function store(Request $request)
  {
    $input = $request->all();
    $process =  Customer::create([
      'name' => $input['name'],
      'gender' => $input['gender'],
      'note' => $input['note'],
      'phone' => $input['phone'],
      'email' => $input['email'],
      'address' => $input['address'],
      'postal_code' => $input['postal_code'],
      'merchant_id' => Auth::user()->merchant_id,
      'active' => isset($input['active']) ? true : false
    ]);
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil ditambahkan.';
    $request->session()->flash('alert', $alert);

    return redirect('customer');
  }

  public function update(Request $request, $id)
  {
    $input = $request->all();
    $process = Customer::findOrFail($id);
    $process->update([
      'name' => $input['name'],
      'gender' => $input['gender'],
      'note' => $input['note'],
      'phone' => $input['phone'],
      'email' => $input['email'],
      'province_id' => isset($input['province_id']) ? $input['province_id'] : null,
      'province_name' => isset($input['province_id']) ? $input['province_name'] : null,
      'city_id' => (isset($input['province_id']) && isset($input['city_id'])) ? $input['city_id'] : null,
      'city_name' => (isset($input['province_id']) && isset($input['city_id'])) ? $input['city_name'] : null,
      'subdistrict_id' => (isset($input['province_id']) && isset($input['city_id']) && isset($input['subdistrict_id'])) ? $input['subdistrict_id'] : null,
      'subdistrict_name' => (isset($input['province_id']) && isset($input['city_id']) && isset($input['subdistrict_id'])) ? $input['subdistrict_name'] : null,
      'address' => $input['address'],
      'postal_code' => $input['postal_code'],
      'active' => isset($input['active']) ? true : false
    ]);
    
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil diupdate.';
    $request->session()->flash('alert', $alert);

    return redirect('customer');
  }

  public function delete($id)
  {
    Customer::destroy($id);
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil dihapus.';
    return redirect()->back();
  }

  public function detail($id)
  {
    $data['_title'] = $this->title;
    $data['customer'] = Customer::with(['sales', 'sales.details'])->findOrFail($id);

    // return collect($data['customer']->sales)->map(function ($item, $key) {
    //   return $item->details;
    // })->collapse()->sum('qty');

    return view('customers.detail', compact('data'));
  }
}
