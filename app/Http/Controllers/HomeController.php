<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sale\Sale;
use Auth;

class HomeController extends Controller
{
  private $title = 'Home';
  public function index()
  {
    // echo Auth::user()->subscription('main')->plan;
    // die();
    $data['_title'] = $this->title;
    return view('home.index', compact('data'));
  }

  public function allSales(Request $request)
  {
    $start = $request->start;
    $end = $request->end;
    $query = Sale::whereBetween('invoice_date', [$start, $end])
      ->where([
        ['deleted_at', null],
        ['payment_status', '2'],
        ['sale_status', '4'],
        ['merchant_id', Auth::user()->merchant_id]
      ])->with(['details', 'details.product'])->get();
    return $query;
  }
}
