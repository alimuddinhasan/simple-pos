<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Merchant\Merchant;
use App\User;
use App\Role;
use Gerardojbaez\Laraplans\Models\Plan;

use Datatables;
use Carbon\Carbon;

class MerchantController extends Controller
{
  private $title = 'Merchants';

  public function index(Request $request)
  {
    $data['_title'] = $this->title;
    return view('merchant.merchant.index', compact('data'));
  }

  public function fetchAll()
  {
    $query = Merchant::where([
      ['deleted_at', null]
    ]);
    return Datatables::of($query)
      ->editColumn('active', function ($data) {
        return $data->active ? '<span class="label label-success">Available</span>' : '<span class="label label-danger">Not Available</span>';
      })
      ->addColumn('action', function ($data) {
        $html = '<a href="'.url('merchant/'.$data->id.'').'" class="btn btn-sm btn-default"><i class="fa fa-search"></i></a>
        <a href="'.url('merchant/'.$data->id.'/edit').'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a>
        <a href="'.url('merchant/'.$data->id.'/delete').'" class="btn btn-sm btn-danger delete-btn"><i class="fa fa-trash"></i></a>';
        return $html;
      })
      ->rawColumns(['active', 'action'])->make(true);
  }

  public function showForm(Request $request, $id = null)
  {
    $data['_title'] = $this->title;

    if ($id) {
      $data['merchant'] = Merchant::findOrFail($id);
      return view('merchant.merchant.form', compact('data'));
    } else {
      return view('merchant.merchant.form', compact('data'));
    }
  }

  public function store(Request $request)
  {
    $input = $request->all();

    $merchant = Merchant::create([
      'name' => $input['name'],
      'email' => $input['email'],
      'phone' => $input['phone'],
      'active' => isset($input['active']) ? true : false
    ]);

    $user = User::create([
      'name' => $input['email'],
      'email' => $input['email'],
      'password' => bcrypt('password'),
      'merchant_id' => $merchant->id,
      'active' => true
    ]);
    $user->roles()->attach(Role::where('name', 'merchant-admin')->first());

    $merchant->newSubscription('main', Plan::find(1))->create();

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil ditambahkan.';
    $request->session()->flash('alert', $alert);

    return redirect('merchant');
  }

  public function update(Request $request, $id)
  {
    $input = $request->all();
    $process = Merchant::findOrFail($id);
    $process->update([
      'name' => $input['name'],
      'phone' => $input['phone'],
      'active' => isset($input['active']) ? true : false
    ]);
    
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil diupdate.';
    $request->session()->flash('alert', $alert);

    return redirect('merchant');
  }

  public function delete($id)
  {
    Merchant::destroy($id);
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil dihapus.';
    return redirect()->back();
  }

  public function detail($id)
  {
    $data['_title'] = $this->title;
    $data['merchant'] = Merchant::findOrFail($id);
    // return $data['merchant']->users()->orderBy('created_at', 'asc')->whereRoleIs('merchant-admin')->first()->subscription('main')->plan;
    return view('merchant.merchant.detail', compact('data'));
  }
}
