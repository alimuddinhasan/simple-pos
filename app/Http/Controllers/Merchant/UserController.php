<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Models\Merchant\Merchant;

use Datatables;
use Carbon\Carbon;
use DB;

class UserController extends Controller
{
  private $title = 'Users';

  public function index(Request $request)
  {
    if (!$request->id) {
      abort(403);
    }
    
    $data['_title'] = $this->title;
    $data['merchant'] = Merchant::findOrFail($request->id);

    return view('merchant.user.index', compact('data'));
  }

  public function fetchAll(Request $request)
  {
    $query = User::where([
      ['deleted_at', null],
      ['merchant_id', $request->id]
    ]);
    return Datatables::of($query)
      ->editColumn('last_login', function ($data) {
        return $data->last_login ? with(new Carbon($data->last_login))->format('d M Y H:i:s') : '';
      })
      ->editColumn('active', function ($data) {
        return $data->active ? '<span class="label label-success">Available</span>' : '<span class="label label-danger">Not Available</span>';
      })
      ->addColumn('merchant', function ($data) {
        return $data->merchant->name;
      })
      ->addColumn('roles', function ($data) {
        $html = '';
        foreach($data->roles as $d) {
          $html .= '<span class="label label-success">'.$d->display_name.'</span>';
        }
        return $html;
      })
      ->addColumn('action', function ($data) {
        $html = '<a href="'.url('merchantUser/'.$data->id.'/changepassword').'" class="btn btn-sm btn-default"><i class="fa fa-key"></i></a>
        <a href="'.url('merchantUser/'.$data->id.'/edit').'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a>
        <a href="'.url('merchantUser/'.$data->id.'/delete').'" class="btn btn-sm btn-danger delete-btn"><i class="fa fa-trash"></i></a>';
        return $html;
      })
      ->rawColumns(['active', 'action', 'roles'])->make(true);
  }

  public function showForm(Request $request, $id = null)
  {
    $data['_title'] = $this->title;
    $datalist['roles'] = Role::where([
      ['deleted_at', null],
      ['active', true]
    ])->whereIn('name', ['merchant-admin', 'merchant-staff'])->get();
    $datalist['merchants'] = Merchant::where([
      ['deleted_at', null],
      ['active', true]
    ])->get();

    if ($id) {
      $data['merchantUser'] = User::findOrFail($id);
      $data['merchant'] = $data['merchantUser']->merchant;
      return view('merchant.user.form', compact('data', 'datalist'));
    } else {
      if (!$request->id) {
        abort(403);
      }
      $data['merchant'] = Merchant::findOrFail($request->id);
      return view('merchant.user.form', compact('data', 'datalist'));
    }
  }

  public function store(Request $request)
  {
    $input = $request->all();
    $user =  User::create([
      'name' => $input['name'],
      'email' => $input['email'],
      'password' => bcrypt('password'),
      'merchant_id' => $input['merchant_id'],
      'active' => isset($input['active']) ? true : false
    ]);
    $user->roles()->attach($input['role']);

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil ditambahkan.';
    $request->session()->flash('alert', $alert);

    return redirect('merchantUser?id='.$input['merchant_id']);
  }

  public function update(Request $request, $id)
  {
    $input = $request->all();
    $user =  User::findOrFail($id);
    $user->update([
      'name' => $input['name'],
      'merchant_id' => $input['merchant_id'],
      'active' => isset($input['active']) ? true : false
    ]);
    $user->roles()->detach();
    $user->roles()->attach($input['role']);
    
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil diupdate.';
    $request->session()->flash('alert', $alert);

    return redirect('merchantUser?id='.$user->merchant->id);
  }

  public function delete($id)
  {
    User::destroy($id);
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil dihapus.';
    return redirect()->back();
  }

  public function showFormPassword($id)
  {
    $data['_title'] = $this->title;
    $data['user'] = User::findOrFail($id);
    $data['merchant'] = $data['user']->merchant;

    return view('merchant.user.changepassword', compact('data'));
  }

  public function updatePassword(Request $request, $id)
  {
    $input = $request->all();
    $user = User::findOrFail($id);
    $user->password = bcrypt($input['password']);
    $user->save();

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Password berhasil diubah.';
    $request->session()->flash('alert', $alert);

    return redirect('merchantUser?id='.$user->merchant->id);
  }
}
