<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Category;

use Datatables;
use Carbon\Carbon;
use Auth;

class CategoryController extends Controller
{
  private $title = 'Kategori';

  public function index(Request $request)
  {
    $data['_title'] = $this->title;
    return view('product.categories.index', compact('data'));
  }

  public function fetchAll()
  {
    $query = Category::where([
      ['deleted_at', null],
      ['merchant_id', Auth::user()->merchant_id]
    ]);
    return Datatables::of($query)
      ->editColumn('active', function ($data) {
        return $data->active ? '<span class="label label-success">Available</span>' : '<span class="label label-danger">Not Available</span>';
      })
      ->addColumn('action', function ($data) {
        $html = '';
        // $html .= ' <a href="'.url('category/'.$data->id.'/detail').'" class="btn btn-xs btn-default"><i class="fa fa-search"></i></a>';
        $html .= ' <a href="'.url('category/'.$data->id.'/edit').'" class="btn btn-xs btn-success"><i class="fa fa-pencil-alt"></i></a>';
        $html .= ' <a href="'.url('category/'.$data->id.'/delete').'" class="btn btn-xs btn-danger delete-btn"><i class="fa fa-trash"></i></a>';
        return $html;
      })
      ->rawColumns(['active', 'action'])->make(true);
  }

  public function showForm(Request $request, $id = null)
  {
    $data['_title'] = $this->title;

    if ($id) {
      $data['category'] = Category::findOrFail($id);
      return view('product.categories.form', compact('data'));
    } else {
      return view('product.categories.form', compact('data'));
    }
  }

  public function store(Request $request)
  {
    $input = $request->all();
    $process =  Category::create([
      'name' => $input['name'],
      'description' => $input['description'],
      'merchant_id' => Auth::user()->merchant_id,
      'active' => isset($input['active']) ? true : false
    ]);

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil ditambahkan.';
    $request->session()->flash('alert', $alert);

    return redirect('category');
  }

  public function update(Request $request, $id)
  {
    $input = $request->all();
    $process = Category::findOrFail($id);
    $process->update([
      'name' => $input['name'],
      'description' => $input['description'],
      'active' => isset($input['active']) ? true : false
    ]);
    
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil diupdate.';
    $request->session()->flash('alert', $alert);

    return redirect('category');
  }

  public function delete($id)
  {
    Category::destroy($id);
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil dihapus.';
    return redirect()->back();
  }
}
