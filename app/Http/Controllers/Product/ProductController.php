<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Product\Category;

use Datatables;
use Carbon\Carbon;
use DB;
use Auth;
use MediaUploader;

class ProductController extends Controller
{
  private $title = 'Produk';

  public function index(Request $request)
  {
    $data['_title'] = $this->title;
    return view('product.products.index', compact('data'));
  }

  public function fetchAll()
  {
    $query = Product::where([
      ['deleted_at', null],
      ['merchant_id', Auth::user()->merchant_id]
    ]);
    return Datatables::of($query)
      ->editColumn('active', function ($data) {
        return $data->active ? '<span class="label label-success">Available</span>' : '<span class="label label-danger">Not Available</span>';
      })
      ->editColumn('unit_price', function ($data) {
        return 'Rp. ' . moneyFormatter($data->unit_price);
      })
      ->addColumn('thumbnail', function ($data) {
        $img = $data->firstMedia('thumbnail') ? $data->firstMedia('thumbnail')->getUrl() : asset('images/no-image-min.jpg') ;
        $html = '<img src="'.$img.'" style="max-width: 50px;" max-height: 25px; >';
        return $html;
      })
      ->addColumn('action', function ($data) {
        $html = '';
        $html .= ' <a href="'.url('product/'.$data->id.'/edit').'" class="btn btn-xs btn-success"><i class="fa fa-pencil-alt"></i></a>';
        $html .= ' <a href="'.url('product/'.$data->id.'/delete').'" class="btn btn-xs btn-danger delete-btn"><i class="fa fa-trash"></i></a>';
        return $html;
      })
      ->rawColumns(['active', 'action', 'thumbnail'])->make(true);
  }

  public function find (Request $request)
  {
    $q = $request->q;

    $query = Product::where([
      ['deleted_at', null],
      ['active', true],
      ['merchant_id', Auth::user()->merchant_id],
      ['name', 'like', '%'.$q.'%']
    ])->take(10)->get();
    
    return $query;
  }

  public function get ($id)
  {
    $query = Product::findOrFail($id);
    return $query;
  }

  public function showForm(Request $request, $id = null)
  {
    $data['_title'] = $this->title;
    $datalist['categories'] = Category::where([
      ['deleted_at', null],
      ['active', true],
      ['merchant_id', Auth::user()->merchant_id]
    ])->get();

    if ($id) {
      $data['product'] = Product::findOrFail($id);
      return view('product.products.form', compact('data', 'datalist'));
    } else {
      return view('product.products.form', compact('data', 'datalist'));
    }
  }

  public function store(Request $request)
  {
    $input = $request->all();
    $product =  Product::create([
      'name' => $input['name'],
      'description' => $input['description'],
      'category_id' => $input['category_id'],
      'weight' => $input['weight'],
      'unit_cost' => $input['unit_cost'],
      'unit_price' => $input['unit_price'],
      'units' => $input['units'] === 'lainnya' ? $input['other_units'] : $input['units'],
      'merchant_id' => Auth::user()->merchant_id,
      'active' => isset($input['active']) ? true : false
    ]);

    if ($request->hasFile('thumbnail')) {
      $media = MediaUploader::fromSource($request->file('thumbnail'))
        ->toDirectory('products')
        ->useHashForFilename()
        ->upload();
      $product->attachMedia($media, ['thumbnail']);
    }

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil ditambahkan.';
    $request->session()->flash('alert', $alert);

    return redirect('product');
  }

  public function update(Request $request, $id)
  {
    $input = $request->all();
    $product =  Product::findOrFail($id);
    $product->update([
      'name' => $input['name'],
      'description' => $input['description'],
      'category_id' => $input['category_id'],
      'weight' => $input['weight'],
      'unit_cost' => $input['unit_cost'],
      'unit_price' => $input['unit_price'],
      'units' => $input['units'] === 'lainnya' ? $input['other_units'] : $input['units'],
      'merchant_id' => Auth::user()->merchant_id,
      'active' => isset($input['active']) ? true : false
    ]);

    if ($request->hasFile('thumbnail')) {
      $media = MediaUploader::fromSource($request->file('thumbnail'))
        ->toDirectory('products')
        ->useHashForFilename()
        ->upload();
      $product->syncMedia($media, ['thumbnail']);
    }
    
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil diupdate.';
    $request->session()->flash('alert', $alert);

    return redirect('product');
  }

  public function delete($id)
  {
    Product::destroy($id);
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil dihapus.';
    return redirect()->back();
  }
}
