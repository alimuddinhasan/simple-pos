<?php

namespace App\Http\Controllers\Sale;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sale\Sale;
use App\Models\Sale\SaleDetail;
use App\Models\Product\Product;
use GuzzleHttp\Client;
use App\Models\Courier\Courier;

use Datatables;
use Auth;
use Carbon\Carbon;

class SaleController extends Controller
{
  private $title = 'Penjualan';

  public function index()
  {
    $data['_title'] = $this->title;
    return view('sale.sales.index', compact('data'));
  }

  public function fetchAll()
  {
    $query = Sale::where([
      ['deleted_at', null],
      ['merchant_id', Auth::user()->merchant_id]
    ]);
    return Datatables::of($query)
      ->addColumn('status', function ($data) {
        $status = '';

        if ($data->payment_status == 1) {
          $status .= '<span class="label label-warning"><i class="fa fa-money"></i> Pending</span> ';
        } else if ($data->payment_status == 2) {
          $status .= '<span class="label label-success"><i class="fa fa-money"></i> Lunas</span> ';
        } else if ($data->payment_status == 3) {
          $status .= '<span class="label label-danger"><i class="fa fa-money"></i> Canceled</span> ';
        }

        if ($data->sale_status == 1) {
          $status .= '<span class="label label-warning"><i class="fa fa-send"></i> Pending</span> ';
        } else if ($data->sale_status == 2) {
          $status .= '<span class="label label-default"><i class="fa fa-send"></i> On Process</span> ';
        } else if ($data->sale_status == 3) {
          $status .= '<span class="label label-default"><i class="fa fa-send"></i> On Delivery</span> ';
        } else if ($data->sale_status == 4) {
          $status .= '<span class="label label-success"><i class="fa fa-send"></i> Selesai</span> ';
        } else if ($data->sale_status == 5) {
          $status .= '<span class="label label-danger"><i class="fa fa-send"></i> Dibatalkan</span> ';
        }

        return $status;
      })
      ->editColumn('total', function ($data) {
        return 'Rp ' . moneyFormatter($data->total);
      })
      ->editColumn('created_by', function ($data) {
        return $data->user->name;
      })
      ->editColumn('invoice_date', function ($data) {
        return $data->invoice_date->format('d-m-Y');
      })
      ->addColumn('action', function ($data) {
        $html = '';
        $html .= '<a href="'.url('sale/'.$data->id).'" class="btn btn-sm btn-default"><i class="fa fa-search"></i></a> ';
        if ($data->sale_status != 4 && $data->sale_status != 5) {
          $html .= '<a href="'.url('sale/'.$data->id.'/cancel').'" class="btn btn-sm btn-danger cancel-btn"><i class="fa fa-ban"></i></a> ';
        }
        return $html;
      })
      ->rawColumns(['status', 'action'])->make(true);
  }

  public function showForm(Request $request, $id = null)
  {
    $data['_title'] = $this->title;
    $datalist['couriers'] = Courier::where([
      ['deleted_at', null],
      ['active', true]
    ])->get();
    return view('sale.sales.form', compact('data', 'datalist'));
  }

  public function store(Request $request)
  {
    $input = $request->all();
    $items = $input['purchaseItems'];

    $sub_total = 0;
    foreach ($items as $d) {
      $sub_total = $sub_total + ((int) $d['unit_price'] * (int) $d['qty']);
    }

    $invoice_no = mt_rand(1000000000, 9999999999);
    while (true) {
      if (!(Sale::where('invoice_no', $invoice_no)->exists())) break;
      $invoice_no = mt_rand(1000000000, 9999999999);
    }

    $shipping_fee = $input['shipping_fee'];
    $process =  Sale::create([
      'invoice_no' => 'SL'.$invoice_no,
      'invoice_date' => Carbon::createFromFormat('d-m-Y', $input['invoice_date']),
      'weight' => $input['weight'],
      'sub_total' => $sub_total,
      'shipping_fee' => $shipping_fee,
      'total' => $sub_total + $shipping_fee,
      'courier' => $input['courier'],
      'courier_services' => $input['courier_services'],
      'payment_status' => $input['payment_status'],
      'sale_status' => 1, // Init to Pending
      'address_data' => $input['address_data'],
      'customer_id' => isset($input['customer_id']) ? $input['customer_id'] : null,
      'created_by' => Auth::user()->id,
      'merchant_id' => Auth::user()->merchant_id,
    ]);

    $total_cost = 0;
    foreach($items as $d) {
      $product = Product::findOrFail($d['product_id']);
      SaleDetail::create([
        'product_id' => $d['product_id'],
        'qty' => $d['qty'],
        'unit_cost' => $product->unit_cost,
        'total_cost' => (int) $product->unit_cost * (int) $d['qty'],
        'unit_price' => $d['unit_price'],
        'total_price' => (int) $d['unit_price'] * (int) $d['qty'],
        'sale_id' => $process->id
      ]);
      $total_cost = $total_cost + (int) $product->unit_cost * (int) $d['qty'];

      $product->update([
        'stock' => $product->stock - (int) $d['qty']
      ]);
    }

    $process->update([
      'total_cost' => $total_cost
    ]);

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil ditambahkan.';
    $request->session()->flash('alert', $alert);

    return ['status' => 'success', 'code' => 200];
  }

  public function detail($id)
  {
    $data['_title'] = $this->title;
    $data['sale'] = Sale::findOrFail($id);
    return view('sale.sales.detail', compact('data'));
  }

  public function cancelTransaction($id)
  {
    $sale = Sale::findOrFail($id);
    if ($sale->sale_status != 3 && $sale->sale_status != 4) {
      $sale->update([
        'payment_status' => 3,
        'sale_status' => 5
      ]);
      foreach($sale->details as $d) {
        $product = Product::findOrFail($d->product_id);
        $product->update([
          'stock' => $product->stock + (int) $d->qty
        ]);
      }
      $alert['type'] = 'success';
      $alert['message'] = '<strong>Success !</strong> Transaksi berhasil dibatalkan.';
      return redirect()->back();
    }
  }

  public function get ($id)
  {
    $query = Sale::with(['details', 'details.product'])->findOrFail($id);
    return $query;
  }

  public function showWaybillForm ($id) 
  {
    $data['_title'] = $this->title;
    $data['sale'] = Sale::findOrFail($id);
    if (!$data['sale']->courier) abort(403);
    return view('sale.sales.form_waybill', compact('data'));
  }

  public function updateWaybill (Request $request, $id) 
  {
    $input = $request->all();
    $process = Sale::findOrFail($id);
    $process->update([
      'waybill_no' => $input['waybill_no']
    ]);

    return redirect('sale/' . $id);
  }

  public function showShippingLabel($id)
  {
    $data['sale'] = Sale::findOrFail($id);
    if (!$data['sale']->courier) abort(403);
    return view('sale.sales.shipping_label', compact('data'));
  }

  public function updateStatus(Request $request, $id)
  {
    $input = $request->all();
    $process = Sale::findOrFail($id);
    $process->update($input);
    return ['input' => $input, 'status' => 'success', 'code' => 200];
  }
}
