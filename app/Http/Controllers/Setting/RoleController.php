<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;

use Datatables;
use Carbon\Carbon;
use DB;

class RoleController extends Controller
{
  private $title = 'Roles';

  public function index(Request $request)
  {
    $data['_title'] = $this->title;
    return view('setting.role.index', compact('data'));
  }

  public function fetchAll()
  {
    $query = Role::where([
      ['deleted_at', null]
    ]);
    return Datatables::of($query)
      ->editColumn('active', function ($data) {
        return $data->active ? '<span class="label label-success">Available</span>' : '<span class="label label-danger">Not Available</span>';
      })
      ->addColumn('action', function ($data) {
        $html = '<a href="'.url('role/'.$data->id.'/edit').'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a>
        <a href="'.url('role/'.$data->id.'/delete').'" class="btn btn-sm btn-danger delete-btn"><i class="fa fa-trash"></i></a>';
        return $html;
      })
      ->rawColumns(['action', 'active'])->make(true);
  }

  public function showForm(Request $request, $id = null)
  {
    $data['_title'] = $this->title;
    $datalist['permissions'] = Permission::all();

    if ($id) {
      try {
        $data['role'] = Role::findOrFail($id);
      } catch (ModelNotFoundException $e) {
        abort(404);
      }
      $role_permissions = collect($data['role']->permissions);
      $data['role_permissions'] = collect($role_permissions->map(function ($item, $key) {
          return $item['id'];
      }));
      return view('setting.role.form', compact('data', 'datalist'));
    } else {
      return view('setting.role.form', compact('data', 'datalist'));
    }
  }

  public function store(Request $request)
  {
    $input = $request->all();
    // return $input;
    $role =  Role::create([
      'name' => $input['name'],
      'display_name' => $input['display_name'],
      'active' => isset($input['active']) ? true : false
    ]);
    $role->permissions()->sync($input['permissions']);
    // $role->attachPermissions($input['permissions']);

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil ditambahkan.';
    $request->session()->flash('alert', $alert);

    return redirect('role');
  }

  public function update(Request $request, $id)
  {
    $input = $request->all();
    // return isset($input['active']) ? 'true' : 'false';
    $role =  Role::findOrFail($id);
    $role->update([
      'name' => $input['name'],
      'display_name' => $input['display_name'],
      'active' => isset($input['active']) ? true : false
      ]);
    // $role->syncPermissions($input['permissions']);
    $role->permissions()->sync($input['permissions']);
    
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil diupdate.';
    $request->session()->flash('alert', $alert);

    return redirect('role');
  }

  public function delete($id)
  {
    Role::destroy($id);
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil dihapus.';
    return redirect()->back();
  }
}
