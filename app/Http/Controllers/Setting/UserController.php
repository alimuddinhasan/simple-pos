<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;

use Datatables;
use Carbon\Carbon;
use DB;
// use Session;

class UserController extends Controller
{
  private $title = 'Users';

  public function index(Request $request)
  {
    $data['_title'] = $this->title;
    return view('setting.user.index', compact('data'));
  }

  public function fetchAll()
  {
    $query = User::where([
      ['deleted_at', null],
      ['merchant_id', null]
    ]);
    return Datatables::of($query)
      ->editColumn('last_login', function ($data) {
        return $data->last_login ? with(new Carbon($data->last_login))->format('d M Y H:i:s') : '';
      })
      ->editColumn('active', function ($data) {
        return $data->active ? '<span class="label label-success">Available</span>' : '<span class="label label-danger">Not Available</span>';
      })
      ->addColumn('roles', function ($data) {
        $html = '';
        foreach($data->roles as $d) {
          $html .= '<span class="label label-success">'.$d->display_name.'</span>';
        }
        return $html;
      })
      ->addColumn('action', function ($data) {
        $html = '<a href="'.url('user/'.$data->id.'/changepassword').'" class="btn btn-sm btn-default"><i class="fa fa-key"></i></a>
        <a href="'.url('user/'.$data->id.'/edit').'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a>
        <a href="'.url('user/'.$data->id.'/delete').'" class="btn btn-sm btn-danger delete-btn"><i class="fa fa-trash"></i></a>';
        return $html;
      })
      ->rawColumns(['active', 'action', 'roles'])->make(true);
  }

  public function checkEmail(Request $request)
  {
    $user = User::where([
      ['email', $request->email],
      ['deleted_at', null]
    ])->first();

    if($user) {
      return 'false';
    } else {
      return 'true';
    }
  }

  public function showForm(Request $request, $id = null)
  {
    $data['_title'] = $this->title;
    $datalist['roles'] = Role::whereIn('name', ['superuser', 'administrator'])->get();

    if ($id) {
      $data['user'] = User::findOrFail($id);
      return view('setting.user.form', compact('data', 'datalist'));
    } else {
      return view('setting.user.form', compact('data', 'datalist'));
    }
  }

  public function store(Request $request)
  {
    $input = $request->all();
    $user =  User::create([
      'name' => $input['name'],
      'email' => $input['email'],
      'password' => bcrypt('password'),
      'active' => isset($input['active']) ? true : false
    ]);
    $user->roles()->attach($input['role']);

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil ditambahkan.';
    $request->session()->flash('alert', $alert);

    return redirect('user');
  }

  public function update(Request $request, $id)
  {
    $input = $request->all();
    $user =  User::findOrFail($id);
    $user->update([
      'name' => $input['name'],
      'active' => isset($input['active']) ? true : false
    ]);
    $user->roles()->detach();
    $user->roles()->attach($input['role']);
    
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil diupdate.';
    $request->session()->flash('alert', $alert);

    return redirect('user');
  }

  public function delete($id)
  {
    User::destroy($id);
    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Data berhasil dihapus.';
    return redirect()->back();
  }

  public function showFormPassword($id)
  {
    $data['_title'] = $this->title;
    $data['user'] = User::findOrFail($id);

    return view('setting.user.changepassword', compact('data'));
  }

  public function updatePassword(Request $request, $id)
  {
    $input = $request->all();
    $user = User::findOrFail($id);
    $user->password = bcrypt($input['password']);
    $user->save();

    $alert['type'] = 'success';
    $alert['message'] = '<strong>Success !</strong> Password berhasil diubah.';
    $request->session()->flash('alert', $alert);

    return redirect('user');
  }
}
