<?php

namespace App\Http\Middleware;

use Closure;

class IsVerivied
{
    public function handle($request, Closure $next)
    {
        if(!auth()->user()->verified){
            Session::flush();
            return redirect('login')->withAlert('Silahkan verifikasi email Anda terlebih dahulu.');
        }
        return $next($request);
    }
}
