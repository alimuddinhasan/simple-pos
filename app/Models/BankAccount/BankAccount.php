<?php

namespace App\Models\BankAccount;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class BankAccount extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'bank_name',
    'account_no',
    'name',
    'active',
    'deleted_at'
  ];
  protected $dates = ['deleted_at'];
}
