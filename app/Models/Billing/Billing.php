<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Billing extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'invoice_no',
    'amount',
    'payment_status',
    'type',
    'plan_id',
    'created_by',
    'merchant_id',
    'expire_at',
  ];
  protected $dates = ['expire_at'];

  public function merchant()
  {
      return $this->belongsTo('App\Models\Merchant\Merchant', 'merchant_id');
  }
}
