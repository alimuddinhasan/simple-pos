<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class BillingDetail extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'type',
    'billing_id',
    'created_by'
  ];
}
