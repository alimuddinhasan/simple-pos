<?php

namespace App\Models\Billing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class PaymentConfirmation extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $table = 'payment_confirmation';

  protected $fillable = [
    'name',
    'amount',
    'status',
    'billing_id',
    'bank_id'
  ];
}
