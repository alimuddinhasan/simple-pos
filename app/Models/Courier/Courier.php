<?php

namespace App\Models\Courier;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Courier extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'code',
    'name',
    'active',
    'waybill_status',
    'deleted_at'
  ];
  protected $dates = ['deleted_at'];

  public function purchase()
  {
    return $this->hasMany('App\Models\Inventory\Purchase', 'courier_id');
  }
}
