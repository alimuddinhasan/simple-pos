<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Customer extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'name', 
    'gender', 
    'phone',  
    'email',  
    'socials', 
    'note', 
    'province_id', 
    'province_name',
    'city_id', 
    'city_name',
    'subdistrict_id', 
    'subdistrict_name',
    'postal_code', 
    'merchant_id', 
    'address', 
    'postal_code', 
    'active', 
    'deleted_at'
  ];
  protected $dates = ['deleted_at'];

  public function sales()
  {
    return $this->hasMany('App\Models\Sale\Sale', 'customer_id');
  }
}
