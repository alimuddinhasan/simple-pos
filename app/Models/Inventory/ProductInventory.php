<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class ProductInventory extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'record_date', 
    'total_cost',
    'note',
    'type', // 1: entry, 2: exit;
    'created_by',
    'merchant_id', 
    'deleted_at'
  ];

  protected $dates = ['deleted_at', 'record_date'];

  public function user()
  {
    return $this->belongsTo('App\User', 'created_by');
  }

  public function purchase()
  {
    return $this->belongsTo('App\Models\Inventory\Purchase', 'purchase_id');
  }

  public function details()
  {
    return $this->hasMany('App\Models\Inventory\ProductInventoryDetail', 'inventory_id');
  }
}
