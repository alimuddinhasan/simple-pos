<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class ProductInventoryDetail extends Model
{
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'qty',
    'unit_cost', 
    'total_cost',
    'product_id', 
    'inventory_id'
  ];

  public function inventory()
  {
    return $this->belongsTo('App\Models\Inventory\ProductInventory', 'inventory_id');
  }

  public function product()
  {
    return $this->belongsTo('App\Models\Product\Product', 'product_id');
  }
}
