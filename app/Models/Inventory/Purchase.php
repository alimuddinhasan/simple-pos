<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Purchase extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'purchase_no', 
    'purchase_date', 
    'received_date',
    'total_cost',
    'courier_id',
    'waybill_no',
    'note',
    'received_by',
    'supplier_id', 
    'merchant_id', 
    'deleted_at'
  ];

  protected $dates = ['deleted_at', 'purchase_date', 'received_date'];

  public function supplier()
  {
    return $this->belongsTo('App\Models\Inventory\Supplier', 'supplier_id');
  }

  public function user()
  {
    return $this->belongsTo('App\User', 'received_by');
  }

  public function courier()
  {
    return $this->belongsTo('App\Models\Courier\Courier', 'courier_id');
  }

  public function details()
  {
    return $this->hasMany('App\Models\Inventory\PurchaseDetail', 'purchase_id');
  }
}
