<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class PurchaseDetail extends Model
{
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'qty',
    'unit_cost', 
    'total_cost', 
    'qty_received',
    'product_id', 
    'purchase_id'
  ];

  public function purchase()
  {
    return $this->belongsTo('App\Models\Inventory\Purchase', 'purchase_id');
  }

  public function product()
  {
    return $this->belongsTo('App\Models\Product\Product', 'product_id');
  }
}
