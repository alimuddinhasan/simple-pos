<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class StockOpname extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $table = 'stock_opname';

  protected $fillable = [
    'record_date',
    'note',
    'created_by',
    'merchant_id', 
    'deleted_at'
  ];

  protected $dates = ['deleted_at', 'record_date'];

  public function user()
  {
    return $this->belongsTo('App\User', 'created_by');
  }

  public function purchase()
  {
    return $this->belongsTo('App\Models\Inventory\Purchase', 'purchase_id');
  }

  public function details()
  {
    return $this->hasMany('App\Models\Inventory\StockOpnameDetail', 'opname_id');
  }
}
