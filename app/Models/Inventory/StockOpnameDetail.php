<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class StockOpnameDetail extends Model
{
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'opening_stock',
    'opening_unit_cost',
    'closing_stock',
    'closing_unit_cost',
    'cost_diff', 
    'stock_diff',
    'product_id', 
    'opname_id'
  ];

  public function opname()
  {
    return $this->belongsTo('App\Models\Inventory\StockOpname', 'opname_id');
  }

  public function product()
  {
    return $this->belongsTo('App\Models\Product\Product', 'product_id');
  }
}
