<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Supplier extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'name', 'address', 'phone', 'merchant_id', 'active', 'deleted_at'
  ];
  protected $dates = ['deleted_at'];

  public function purchases()
  {
    return $this->hasMany('App\Models\Inventory\Purchase', 'supplier_id');
  }
}
