<?php

namespace App\Models\Merchant;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;
use Gerardojbaez\Laraplans\Contracts\PlanSubscriberInterface;
use Gerardojbaez\Laraplans\Traits\PlanSubscriber;

class Merchant extends Model implements PlanSubscriberInterface
{
  use SoftDeletes;
  use Uuids;
  use PlanSubscriber;
  public $incrementing = false;
  protected $fillable = [
    'name', 
    'email', 
    'phone', 
    'socials',
    'province_id', 
    'province_name', 
    'city_id', 
    'city_name', 
    'subdistrict_id', 
    'subdistrict_name', 
    'address', 
    'postal_code', 
    'parent_id', 
    'active', 
    'deleted_at'
  ];
  protected $dates = ['deleted_at'];
  
  public function users()
  {
    return $this->hasMany('App\User', 'merchant_id');
  }

  public function activeBilling()
  {
    return $this->hasMany('App\Models\Billing\Billing', 'merchant_id')->where('payment_status', 1);
  }

  public function billings()
  {
    return $this->hasMany('App\Models\Billing\Billing', 'merchant_id');
  }


}
