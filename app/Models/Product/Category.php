<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Category extends Model
{
  use SoftDeletes;
  use Uuids;
  
  public $incrementing = false;
  protected $fillable = [
    'name', 'description', 'merchant_id', 'active', 'deleted_at'
  ];
  protected $dates = ['deleted_at'];
}
