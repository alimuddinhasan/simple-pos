<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;
use Plank\Mediable\Mediable;

class Product extends Model
{
  use SoftDeletes;
  use Uuids;
  use Mediable;

  public $incrementing = false;
  protected $fillable = [
    'name',
    'description',
    'stock',
    'unit_cost',
    'unit_price',
    'merchant_id',
    'category_id',
    'weight',
    'units',
    'active',
    'deleted_at'
  ];
  protected $dates = ['deleted_at'];

  public function purchaseDetails()
  {
    return $this->hasMany('App\Models\Inventory\PurchaseDetail', 'product_id');
  }
}
