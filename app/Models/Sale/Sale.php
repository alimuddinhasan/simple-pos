<?php

namespace App\Models\Sale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Sale extends Model
{
  use SoftDeletes;
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'invoice_no', 
    'invoice_date', 
    'weight', 
    'total_cost', 
    'sub_total', 
    'shipping_fee',
    'total',
    'note',
    'courier',
    'courier_services',
    'waybill_no',
    'address_data',
    'payment_status',
    'sale_status',
    'customer_id',
    'created_by', 
    'merchant_id', 
    'deleted_at'
  ];

  protected $dates = ['deleted_at', 'purchase_date', 'invoice_date'];
  protected $casts = ['address_data' => 'json'];

  public function customer()
  {
    return $this->belongsTo('App\Models\Customer\Customer', 'customer_id');
  }

  public function user()
  {
    return $this->belongsTo('App\User', 'created_by');
  }

  public function details()
  {
    return $this->hasMany('App\Models\Sale\SaleDetail', 'sale_id');
  }
}
