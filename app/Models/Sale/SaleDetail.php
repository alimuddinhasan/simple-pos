<?php

namespace App\Models\Sale;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class SaleDetail extends Model
{
  use Uuids;
  public $incrementing = false;

  protected $fillable = [
    'qty',
    'unit_cost', 
    'total_cost', 
    'unit_price', 
    'total_price', 
    'product_id', 
    'sale_id'
  ];

  public function sale()
  {
    return $this->belongsTo('App\Models\Sale\Sale', 'sale_id');
  }

  public function product()
  {
    return $this->belongsTo('App\Models\Product\Product', 'product_id');
  }
}
