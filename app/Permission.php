<?php

namespace App;

use Laratrust\Models\LaratrustPermission;
use App\Traits\Uuids;

class Permission extends LaratrustPermission
{
    use Uuids;
    //
    public $incrementing = false;
}
