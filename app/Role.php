<?php

namespace App;

use Laratrust\Models\LaratrustRole;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Role extends LaratrustRole
{
    use SoftDeletes;
    use Uuids;
    protected $fillable = [
        'name', 'display_name', 'active', 'deleted_at'
    ];
    public $incrementing = false;
}
