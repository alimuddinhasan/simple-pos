<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;
use Gerardojbaez\Laraplans\Contracts\PlanSubscriberInterface;
use Gerardojbaez\Laraplans\Traits\PlanSubscriber;

class User extends Authenticatable implements PlanSubscriberInterface
{
    use LaratrustUserTrait;
    use Notifiable;
    use SoftDeletes;
    use Uuids;
    use PlanSubscriber;

    public $incrementing = false;
    protected $fillable = [
        'name', 'email', 'password', 'active', 'deleted_at', 'last_login', 'merchant_id'
    ];
    protected $dates = ['last_login', 'deleted_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant\Merchant', 'merchant_id');
    }

    public function purchase()
    {
        return $this->belongsTo('App\Models\Inventory\Purchase', 'received_by');
    }
}
