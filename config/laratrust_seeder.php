<?php

return [
    'role_structure' => [
        'superuser' => [
            'users' => 'c,r,u,d',
            'roles' => 'c,r,u,d',
            'merchants' => 'c,r,u,d',
            'merchant-users' => 'c,r,u,d',
            'invoice' => 'c,r,u,d'
        ],
        'administrator' => [
            'users' => 'c,r,u,d',
            'merchants' => 'c,r,u,d',
            'merchant-users' => 'c,r,u,d'
        ],
        'merchant-admin' => [
            'categories' => 'c,r,u,d',
            'products' => 'c,r,u,d',
            'suppliers' => 'c,r,u,d',
            'customers' => 'c,r,u,d',
            'purchases' => 'c,r,u,d,rc',
            'inventories' => 'c,r,u,d',
            'promos' => 'c,r,u,d',
            'sales' => 'c,r,u,d',
            'user-profile' => 'r,u',
            'bussiness-profile' => 'r,u',
            'billing' => 'r'
        ],
        'merchant-staff' => [
            'customers' => 'c,r,u',
            'purchases' => 'r,rc',
            'user-profile' => 'r,u'
        ]
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete',
        'rc' => 'receive'
    ]
];
