<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('purchase_no')->nullable();
            $table->date('purchase_date');
            $table->date('received_date')->nullable();
            $table->integer('total_cost')->default(0);
            $table->string('waybill_no')->nullable();
            $table->text('note')->nullable();
            $table->json('others_fee')->nullable();
            
            $table->uuid('courier_id')->nullable();
            $table->foreign('courier_id')->references('id')->on('couriers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('received_by')->nullable();
            $table->foreign('received_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('merchant_id')->nullable();
            $table->foreign('merchant_id')->references('id')->on('merchants')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('supplier_id')->nullable();
            $table->foreign('supplier_id')->references('id')->on('suppliers')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->dropForeign('purchases_courier_id_foreign');
            $table->dropForeign('purchases_merchant_id_foreign');
            $table->dropForeign('purchases_supplier_id_foreign');
            $table->dropForeign('purchases_received_by_foreign');
        });
        Schema::dropIfExists('purchases');
    }
}
