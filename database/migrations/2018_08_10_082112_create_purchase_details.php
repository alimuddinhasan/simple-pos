<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('qty')->default(0);
            $table->integer('unit_cost')->default(0);
            $table->integer('total_cost')->default(0);
            $table->integer('qty_received')->default(0);

            $table->uuid('purchase_id')->nullable();
            $table->foreign('purchase_id')->references('id')->on('purchases')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_details', function (Blueprint $table) {
            $table->dropForeign('purchase_details_purchase_id_foreign');
            $table->dropForeign('purchase_details_product_id_foreign');
        });
        Schema::dropIfExists('purchase_details');
    }
}
