<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductInventories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_inventories', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->date('record_date');
            $table->json('others_fee')->nullable();
            $table->integer('total_cost')->default(0);
            $table->text('note')->nullable();
            $table->string('type', 5)->nullable(); // 1: entry, 2: exit;
            
            $table->uuid('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('merchant_id')->nullable();
            $table->foreign('merchant_id')->references('id')->on('merchants')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_inventories', function (Blueprint $table) {
            $table->dropForeign('product_inventories_created_by_foreign');
            $table->dropForeign('product_inventories_merchant_id_foreign');
        });
        Schema::dropIfExists('product_inventories');
    }
}
