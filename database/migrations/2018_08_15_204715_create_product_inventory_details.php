<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductInventoryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_inventory_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('qty')->default(0);
            $table->integer('unit_cost')->default(0);
            $table->integer('total_cost')->default(0);

            $table->uuid('inventory_id')->nullable();
            $table->foreign('inventory_id')->references('id')->on('product_inventories')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_inventory_details', function (Blueprint $table) {
            $table->dropForeign('product_inventory_details_inventory_id_foreign');
            $table->dropForeign('product_inventory_details_product_id_foreign');
        });
        Schema::dropIfExists('product_inventory_details');
    }
}
