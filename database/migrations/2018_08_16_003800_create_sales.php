<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->date('invoice_date');
            $table->string('invoice_no')->unique();
            $table->integer('weight')->default(1000);
            $table->integer('total_cost')->default(0);
            $table->integer('sub_total')->default(0);
            $table->integer('shipping_fee')->default(0);
            $table->integer('total')->default(0);
            $table->text('note')->nullable();
            $table->string('courier')->nullable();
            $table->string('courier_services')->nullable();
            $table->string('waybill_no')->nullable();
            $table->string('payment_status')->nullable(); // 1: Pending, 2: Paid, 3: Cancel
            $table->string('sale_status')->nullable(); // 1: Pending, 2: On Process, 3: On Delivery, 4: Finished, 5: Canceled
            $table->json('address_data')->nullable();
            
            $table->uuid('courier_id')->nullable();
            $table->foreign('courier_id')->references('id')->on('couriers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('customer_id')->nullable();
            $table->foreign('customer_id')->references('id')->on('customers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('merchant_id')->nullable();
            $table->foreign('merchant_id')->references('id')->on('merchants')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropForeign('sales_courier_id_foreign');
            $table->dropForeign('sales_customer_id_foreign');
            $table->dropForeign('sales_created_by_foreign');
            $table->dropForeign('sales_merchant_id_foreign');
        });
        Schema::dropIfExists('sales');
    }
}
