<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('qty')->default(0);
            $table->integer('unit_cost')->default(0);
            $table->integer('total_cost')->default(0);
            $table->integer('unit_price')->default(0);
            $table->integer('total_price')->default(0);

            $table->uuid('sale_id')->nullable();
            $table->foreign('sale_id')->references('id')->on('sales')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_details', function (Blueprint $table) {
            $table->dropForeign('sale_details_sale_id_foreign');
            $table->dropForeign('sale_details_product_id_foreign');
        });
        Schema::dropIfExists('sale_details');
    }
}
