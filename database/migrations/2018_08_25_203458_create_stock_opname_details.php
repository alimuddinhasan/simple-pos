<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockOpnameDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_opname_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('opening_stock')->nullable();
            $table->integer('opening_unit_cost')->nullable();
            $table->integer('closing_stock')->nullable();
            $table->integer('closing_unit_cost')->nullable();
            $table->integer('stock_diff')->nullable();
            $table->integer('cost_diff')->nullable();

            $table->uuid('opname_id')->nullable();
            $table->foreign('opname_id')->references('id')->on('stock_opname')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_opname_details');
    }
}
