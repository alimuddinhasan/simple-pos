<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('invoice_no');
            $table->integer('amount')->default(0);
            $table->tinyInteger('payment_status')->nullable(); // 1: Pending, 2: Verification Progress, 3: Success, 4: Void
            $table->tinyInteger('type')->nullable(); // 1: Upgrade, 2: Renew
            
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('plans')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('merchant_id')->nullable();
            $table->foreign('merchant_id')->references('id')->on('merchants')
                ->onUpdate('cascade')->onDelete('cascade');
            
            $table->datetime('expire_at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billings', function (Blueprint $table) {
            $table->dropForeign('billings_plan_id_foreign');
            $table->dropForeign('billings_created_by_foreign');
            $table->dropForeign('billings_merchant_id_foreign');
        });
        Schema::dropIfExists('billings');
    }
}
