<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->tinyInteger('type')->nullable(); // 1: Request, 2: Confirm Paid by Customer, 3: Confirm Paid by Admin
            
            $table->uuid('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('billing_id')->nullable();
            $table->foreign('billing_id')->references('id')->on('billings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_details', function (Blueprint $table) {
            $table->dropForeign('billing_details_billing_id_foreign');
            $table->dropForeign('billing_details_created_by_foreign');
        });
        Schema::dropIfExists('billing_details');
    }
}
