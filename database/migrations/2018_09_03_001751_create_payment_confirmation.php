<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentConfirmation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_confirmation', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name');
            $table->integer('amount');
            $table->tinyInteger('status')->nullable(); // 1: Pending, 2: Approved, 3: Rejected

            $table->uuid('billing_id')->nullable();
            $table->foreign('billing_id')->references('id')->on('billings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->uuid('bank_id')->nullable();
            $table->foreign('bank_id')->references('id')->on('bank_accounts')
                ->onUpdate('cascade')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_confirmation', function (Blueprint $table) {
            $table->dropForeign('payment_confirmation_billing_id_foreign');
            $table->dropForeign('payment_confirmation_bank_id_foreign');
        });
        Schema::dropIfExists('payment_confirmation');
    }
}
