<?php

use Illuminate\Database\Seeder;

class BankAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $couriers = [
            [
                'bank_name' => 'BCA (Bank Central Asia)',
                'account_no' => '8161158658',
                'name' => 'Alimuddin Hasan Al Kabir',
                'active' => true
            ],
            [
                'bank_name' => 'BNI (Bank Negara Indonesia)',
                'account_no' => '0303844373',
                'name' => 'Alimuddin Hasan Al Kabir',
                'active' => true
            ]
        ];
        foreach ($couriers as $d) {
            \App\Models\BankAccount\BankAccount::create($d);
        }
    }
}
