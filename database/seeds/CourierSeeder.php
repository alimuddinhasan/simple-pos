<?php

use Illuminate\Database\Seeder;

class CourierSeeder extends Seeder
{
    public function run()
    {
        $couriers = [
            [
                'code' => 'jne',
                'name' => 'JNE',
                'active' => true,
                'waybill_status' => true
            ],
            [
                'code' => 'pos',
                'name' => 'POS Indonesia',
                'active' => true,
                'waybill_status' => true
            ],
            [
                'code' => 'tiki',
                'name' => 'TIKI',
                'active' => true,
                'waybill_status' => true
            ],
            [
                'code' => 'jnt',
                'name' => 'J&T',
                'active' => true,
                'waybill_status' => true
            ],
            [
                'code' => 'jet',
                'name' => 'JET Express',
                'active' => true,
                'waybill_status' => true
            ],
            [
                'code' => 'sicepat',
                'name' => 'Si Cepat',
                'active' => true,
                'waybill_status' => true
            ]
        ];
        foreach ($couriers as $d) {
            \App\Models\Courier\Courier::create($d);
        }
    }
}
