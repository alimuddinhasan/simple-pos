<?php

use Illuminate\Database\Seeder;
use Gerardojbaez\Laraplans\Models\Plan;
use Gerardojbaez\Laraplans\Models\PlanFeature;

class PlanSeeder extends Seeder
{
    public function run()
    {
        $plan = Plan::create([
            'name' => 'Basic',
            'description' => 'Tipe akun Gratis',
            'price' => 0,
            'interval' => 'year',
            'interval_count' => 10,
            'trial_period_days' => 14,
            'sort_order' => 1,
        ]);
        
        $plan = Plan::create([
            'name' => 'Pro Bulanan',
            'description' => 'Tipe akun Pro bulanan',
            'price' => 105000,
            'interval' => 'month',
            'interval_count' => 1,
            'trial_period_days' => 14,
            'sort_order' => 2,
        ]);

        $plan = Plan::create([
            'name' => 'Pro Tahunan',
            'description' => 'Tipe akun Pro tahunan',
            'price' => 1134000,
            'interval' => 'month',
            'interval_count' => 1,
            'trial_period_days' => 14,
            'sort_order' => 3,
        ]);
        
        // $plan->features()->saveMany([
        //     new PlanFeature(['code' => 'transaction', 'value' => 10, 'sort_order' => 1])
        // ]);
    }
}
