
var purchaseItems = [];

$('#product_id')
  .select2({
    placeholder: 'Pilih produk',
    theme: "bootstrap",
    ajax: {
      url: '/product/find',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results: $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  })
  .on('change', function () {
    var productId = $("#product_id").find(":selected").val();
    if (productId) {
      $.ajax({
        url: '/product/' + productId,
        success: function (result) {
          result.product_id = result.id;
          delete result.id;
          var qty = $("#qty").val();
          if (qty >= 1) {
            var productIndex = _.findIndex(purchaseItems, { product_id: result.product_id});
            var newProduct = result;
            newProduct.qty = Number(qty);
            if (productIndex >= 0) {
              newProduct = purchaseItems[productIndex];
              newProduct.qty = Number(newProduct.qty) + Number(qty);
            }
            var content = `<td>${newProduct.name}</td>
              <td>${newProduct.unit_price}</td>
              <td><input type="number" class="form-control detail_qty" value="${newProduct.qty}"  /></td>
              <td class="weight-total">${ Number(newProduct.weight) * Number(newProduct.qty) }</td>
              <td class="sub-total">${ Number(newProduct.unit_price) * Number(newProduct.qty) }</td>
              <td>
                <button type="button" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-trash"></i> Hapus</button>
              </td>`;
            if (productIndex >= 0) {
              purchaseItems[productIndex] = newProduct;
              $('#itemsTable tbody tr:eq('+productIndex+')').html(content)
            } else {
              purchaseItems.push(newProduct);
              $('#itemsTable > tbody:last-child').append(`<tr>${content}</tr>`);
            }
            updateTotal();
            initSalesForm();
          } else {
            alert('Masukkan jumlah barang.')
          }
        }
      });
    }
  });

function getWeightTotal() {
  var weight = 0;
  $(".weight-total").each(function () {
    weight = weight + Number($(this).text());
  });
  if (weight === 0) {
    weight = 1000;
  }
  return Number(weight);
}

function updateTotal() {
  var subTotal = getSubTotal();
  var shippingFee = getShippingFee();
  total = subTotal + shippingFee

  updateShippingFee(shippingFee);
  updateSubTotal(subTotal);
  $("#total").html(numeral(total).format('$0,0'));
}

function populateShippingServices(params = {}) {
  if (params.origin && params.destination && params.weight) {
    $('#courier_services').empty();
    updateShippingFee();
    $.ajax({
      type: 'POST',
      url: '/courier/cost',
      data: {
        _token: $('meta[name=csrf-token]').attr('content'),
        weight: params.weight,
        courier: 'jne:pos:tiki:jnt:jet:sicepat',
        origin: params.origin,
        destination: params.destination,
        originType: 'subdistrict',
        destinationType: 'subdistrict'
      },
      success: function (result) {
        if (result) {
          _.each(result, function(v) {
            if (v.costs.length < 1) return
            var cost = _.map(v.costs, function (x) {
              return { 
                service: x.service,
                cost: x.cost[0]
              }
            })
              var content = `
                <td rowspan="${cost.length}" class="text-center"><img src="/images/couriers/${v.code}-min.png" class="courier-logo"></td>
                <td><input type="radio" name="courier"></td>
                <td>${cost[0].service}</td>
                <td>${_.includes(cost[0].cost.etd.toLowerCase(), 'hari') ? cost[0].cost.etd.toLowerCase() : cost[0].cost.etd || '-' + ' hari'}</td>
                <td>${cost[0].cost.value}</td>
              `
              $('#courier-tbl > tbody:last-child').append(`<tr>${content}</tr>`)
            if (cost.length > 1) {
              _.each(cost, function(x, i) {
                if (i === 0) return
                content = `
                  <td><input type="radio" name="courier"></td>
                  <td>${x.service}</td>
                  <td>${_.includes(x.cost.etd.toLowerCase(), 'hari') ? x.cost.etd.toLowerCase() : x.cost.etd + ' hari'}</td>
                  <td>${x.cost.value}</td>
                `
                $('#courier-tbl > tbody:last-child').append(`<tr>${content}</tr>`)
              })
            }
          })
        }
      }
    });
  }
}

$('#courier_services').on('change', function () {
  updateTotal()
})

function getSubTotal() {
  var subTotal = 0
  $(".sub-total").each(function () {
    subTotal = subTotal + Number($(this).text());
  })
  return subTotal
}

function updateSubTotal (subTotal = 0) {
  $("#sub-total").html(numeral(subTotal).format('$0,0'));
}

function getShippingFee() {
  var shippingFee = 0
  var selectedService = $('#courier_services').find(":selected").val()
  if (selectedService) {
    shippingFee = Number(selectedService)
  }
  return shippingFee
}

function updateShippingFee(cost = 0) {
  $("#shipping-fee").html(numeral(cost).format('$0,0'))
}

$("#itemsTable")
  .on('input', '.detail_qty', function () {
    var qty = $(this).val();
    var index = $(this).parent().parent().index();
    purchaseItems[index].qty = qty;
    $(this).parent().next().html(Number(purchaseItems[index].weight) * Number(purchaseItems[index].qty));
    $(this).parent().next().next().html(Number(purchaseItems[index].unit_price) * Number(purchaseItems[index].qty));
    updateTotal();
  })
  .on('click', '.btn-delete', function (e) {
    e.preventDefault();
    var del = confirm('Are you sure delete this data?');
    if (del == true) {
      var selectedRow = $(this).parent().parent();
      purchaseItems.splice(selectedRow.index(), 1);
      selectedRow.remove();
      updateTotal();
    }
  });

$('#customer_id')
  .select2({
    placeholder: 'Pilih customer',
    theme: "bootstrap",
    ajax: {
      url: '/customer/find',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results: $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  })
  .on('change', function () {
    var customerId = $("#customer_id").find(":selected").val();
    if (customerId) {
      $.ajax({
        url: '/customer/' + customerId,
        success: function (result) {
          $("#recipient_name").val(result.name);
          $("#phone").val(result.phone);
          $("#address").val(result.address);
          $("#postal_code").val(result.postal_code);
          populateProvinces(result.province_id).then(() => {
            populateCities(result.province_id, result.city_id).then(() => {
              populateSubdistricts(result.city_id, result.subdistrict_id);
            })
          })
        }
      });
    }
  });

$('#province_id')
  .select2({
    theme: "bootstrap"
  })
  .on('change', function () {
    $('#city_id').empty();
    $('#city_name').empty();
    $("#city-group").hide();
    $('#subdistrict_id').empty();
    $('#subdistrict_name').empty();
    $("#subdistrict-group").hide();
    var provinceId = $("#province_id").find(":selected").val();
    var provinceName = $("#province_id").find(":selected").text();
    $("#province_name").val(provinceName);
    if (provinceId) {
      populateCities(provinceId);
    }
  });

$('#city_id')
  .select2({
    theme: "bootstrap"
  })
  .on('change', function () {
    $('#subdistrict_id').empty()
    $('#subdistrict_name').empty();
    $("#subdistrict-group").hide();
    var cityId = $("#city_id").find(":selected").val();
    var cityName = $("#city_id").find(":selected").text();
    $("#city_name").val(cityName);
    if (cityId) {
      populateSubdistricts(cityId)
    }
  });

$('#subdistrict_id')
  .select2({
    theme: "bootstrap"
  })
  .on('change', function () {
    var subdistrictId = $("#subdistrict_id").find(":selected").val()
    var subdistrictName = $("#subdistrict_id").find(":selected").text()
    $("#subdistrict_name").val(subdistrictName)

    var originId = $('meta[name=subdistrict-id]').attr('content')
    var provinceId = $("#province_id").val()
    var cityId = $("#city_id").val()
    var weight = 0
    $(".weight-total").each(function () {
      weight = weight + Number($(this).text())
    })
    if (weight === 0) {
      weight = 1000
    }

    if (provinceId && cityId && subdistrictId) {
      populateShippingServices({origin: originId, destination: subdistrictId, weight: weight})
    }
  })

$("#courier")
  .on('change', function () {
    var originId = $('meta[name=subdistrict-id]').attr('content')
    var courier = $("#courier").find(":selected").val();
    var provinceId = $("#province_id").val();
    var cityId = $("#city_id").val();
    var subdistrictId = $("#subdistrict_id").val();
    if (courier) {
      if (provinceId && cityId && subdistrictId) {
        var weight = 0;
        $(".weight-total").each(function () {
          weight = weight + Number($(this).text());
        });
        if (weight === 0) {
          weight = 1000;
        }
  
        if (provinceId && cityId && subdistrictId) {
          populateShippingServices({courier: courier, origin: originId, destination: subdistrictId, weight: weight})
        }
      } else {
        alert('Lengkapi alamat pengiriman.')
        $(this).val('').change();
      }
    } else {

    }

  })

function populateProvinces (provinceId = null) {
  $('#province_id').empty()
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: '/courier/province',
      success: function (result) {
        $('#province_id').append('<option value="">-- Pilih Provinsi --</option>');
        $.each(result, function(i, d) {
          $('#province_id').append('<option value="' + d.province_id + '">' + d.province + '</option>');
        });
        if (provinceId) {
          $("#province_id").val(provinceId).change();
        }
        resolve()
      },
      error: function (err) {
        reject(err)
      }
    });
  })
}

function populateCities (provinceId, cityId = null) {
  $('#city_id').empty()
  return new Promise(function (resolve, reject) {
    if (provinceId) {
      $.ajax({
        url: '/courier/city',
        data: { province_id: provinceId },
        success: function (result) {
          $("#city-group").show();
          $('#city_id').append('<option value="">-- Pilih Kota / Kab. --</option>');
          $.each(result, function(i, d) {
            $('#city_id').append('<option value="' + d.city_id + '">' + d.type + ' ' + d.city_name + '</option>');
          });
          if (cityId) {
            $("#city_id").val(cityId).change();
          }
          resolve()
        },
        error: function (err) {
          reject(err)
        }
      });
    }
  })
}

function populateSubdistricts (cityId, subdistrictId = null) {
  $('#subdistrict_id').empty()
  return new Promise(function (resolve, reject) {
    if (cityId) {
      $.ajax({
        url: '/courier/subdistrict',
        data: {city_id: cityId},
        success: function (result) {
          $("#subdistrict-group").show();
          $('#subdistrict_id').append('<option value="">-- Pilih Kecamatan --</option>');
          $.each(result, function(i, d) {
            $('#subdistrict_id').append('<option value="' + d.subdistrict_id + '">' + d.subdistrict_name + '</option>');
          });
          if (subdistrictId) {
            $("#subdistrict_id").val(subdistrictId).change();
          }
          resolve()
        },
        error: function(err) {
          reject(err)
        }
      });
    } else {
      reject('no city id')
    }
  })
}

function initSalesForm () {
  $("#qty").val(1);
  $("#product_id").empty();
}

$("#invoice_date").daterangepicker({
  singleDatePicker: true,
  showDropdowns: true,
  locale: {
    format: 'DD-MM-YYYY'
  }
});

$("#form").on('submit', function (e) {
  e.preventDefault();
  if (!$('#customer_id').find(':selected').val() && $('#recipient_name').val()) {
    alert('Customer tidak dipilih, apakah Anda ingin menyimpan nama penerima sebagai customer baru?')
  }
  if($(this).valid()) {
    $("#submit").attr("disabled", "disabled");
    if(purchaseItems.length > 0) {
      $.ajax({
        type: 'POST',
        url: '/sale',
        data: {
          _token: $('meta[name=csrf-token]').attr('content'),
          invoice_date: $('#invoice_date').val(),
          weight: getWeightTotal(),
          sub_total: getSubTotal(),
          shipping_fee: getShippingFee(),
          courier: $('#courier').find(':selected').val(),
          courier_services: $('#courier_services').find(':selected').text(),
          payment_status: $('#payment_status').find(':selected').val(),
          address_data: {
            recipient_name: $('#recipient_name').val(),
            phone: $('#phone').val(),
            province_id: $('#province_id').find(':selected').val(),
            province_name: $('#province_name').val(),
            city_id: $('#city_id').find(':selected').val(),
            city_name: $('#city_name').val(),
            subdistrict_id: $('#subdistrict_id').find(':selected').val(),
            subdistrict_name: $('#subdistrict_name').val(),
            address: $('#address').val(),
            postal_code: $('#postal_code').val()
          },
          customer_id: $('#customer_id').find(':selected').val(),
          purchaseItems: purchaseItems
        },
        success: function (result) {
          console.log(result)
          window.location.href = '/sale';
        },
        error: function (err) {
          console.log(err);
          $("#submit").removeAttr("disabled");
        },
        dataType: 'json'
      });
    } else {
      alert('Anda belum menambahkan item.')
      $("#submit").removeAttr("disabled");
    }
  }
});

function isCourierSelected() {
  var isSelected = false
  var courierSelected = $("#courier").find(":selected").val()
  if (courierSelected) {
    isSelected = true
  }
  return isSelected
}

$(document).ready( function () {
  $("#city_id").parent().hide();
  $("#subdistrict_id").parent().hide();
  $(".shipping-group").hide();
  initSalesForm();
  populateProvinces()
  
  $("#form").validate({
    rules: {
      invoice_date: "required",
      recipient_name: {
        required: {
          depends: isCourierSelected
        }
      },
      phone: {
        required: {
          depends: isCourierSelected
        },
        digits: true
      },
      province_id: {
        required: {
          depends: isCourierSelected
        }
      },
      city_id: {
        required: {
          depends: isCourierSelected
        }
      },
      subdistrict_id: {
        required: {
          depends: isCourierSelected
        }
      },
      address: {
        required: {
          depends: isCourierSelected
        }
      },
      postal_code: {
        required: {
          depends: isCourierSelected
        },
        digits: true
      }
    },
    messages: {
      invoice_date: "Masukkan tanggal transaksi.",
      recipient_name: {
        required: "Masukkan nama penerima"
      },
      phone: {
        required: "Masukkan nomor telp / hp penerima",
        digits: "Nomor telp / hp tidak valid"
      },
      province_id: {
        required: "Masukkan provinsi"
      },
      city_id: {
        required: "Masukkan kota / kabupaten"
      },
      subdistrict_id: {
        required: "Masukkan kecamatan"
      },
      address: {
        required: "Masukkan alama"
      },
      postal_code: {
        required: "Masukkan kode pos",
        digits: "Kode pos tidak valid"
      }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".form-group" ).addClass( "has-error" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).removeClass( "has-error" );
    }
  });
});

function initShippingDetailPage() {
  $("#")
  $("#courier").val('').change();
  $("#courier_services").empty();
  $("#courier_services").parent().hide();
}

$('#pengiriman-tab-btn').on('click', function (e) {
  if(purchaseItems.length > 0) {
    updateShippingFee()
    initShippingDetailPage()
  } else {
    alert('Anda belum menambahkan item.')
    e.preventDefault()
    return false
  }
})

$('#detail-barang-tab-btn').on('click', function (e) {
  var goBack = confirm('Anda yakin kembali? Detail Pengiriman akan di-reset');
  if (goBack) {
    updateShippingFee()
    initShippingDetailPage()
    updateTotal()
  } else {
    e.preventDefault()
    return false
  }
})

$("#next").on('click', function (e) {
  e.preventDefault();
  $('#pengiriman-tab-btn').trigger('click')
})

$("#back").on('click', function (e) {
  e.preventDefault();
  $('#detail-barang-tab-btn').trigger('click')
})