var Shipping = (function () {
  "use strict"

  function shippingCostFind (params = {}) {
    return new Promise (function (resolve, reject) {
      if (params.origin && params.destination && params.weight) {
        const courier = 'jne:pos:tiki:jnt:jet:sicepat'
        if (params.courier) courier = params.courier
        $.ajax({
          type: 'POST',
          url: '/courier/cost',
          data: {
            _token: $('meta[name=csrf-token]').attr('content'),
            weight: params.weight,
            courier: courier,
            origin: params.origin,
            destination: params.destination,
            originType: 'subdistrict',
            destinationType: 'subdistrict'
          },
          success: function (result) {
            resolve(result)
          },
          error: function (error) {
            reject(error)
          }
        })
      }
    })
  }

  function provinceFind (params = {}) {
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: '/courier/province',
        success: function (result) {
          resolve(result)
        },
        error: function (error) {
          reject(error)
        }
      })
    })
  }
  function cityFind (params = {}) {
    return new Promise(function (resolve, reject) {
      const provinceId = params.provinceId
      if (provinceId) {
        $.ajax({
          url: '/courier/city',
          data: {
            province_id: provinceId
          },
          success: function (result) {
            resolve(result)
          },
          error: function (error) {
            reject(error)
          }
        })
      } else {
        reject('No province id provided')
      }
    })
  }

  function subdistrictFind (params = {}) {
    return new Promise(function (resolve, reject) {
      const cityId = params.cityId
      if (cityId) {
        $.ajax({
          url: '/courier/subdistrict',
          data: {
            city_id: cityId
          },
          success: function (result) {
            resolve(result)
          },
          error: function (error) {
            reject(error)
          }
        })
      } else {
        reject('No city id provided')
      }
    })
  }

  return {
    shippingCostFind,
    provinceFind,
    cityFind,
    subdistrictFind
  }
  
})()