<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ isset($data) ? $data['_title'] ? $data['_title'] : '' : '' }}</title>
    <!-- Common Plugins -->
    <link href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    @yield('styles')

    <!-- Custom Css-->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('loading/jquery.loading.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- ============================================================== -->
    <!-- 						Topbar Start 							-->
    <!-- ============================================================== -->
    <div class="top-bar primary-top-bar">
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <a class="admin-logo" href="index.html">
              <h1>
                <img alt="" src="{{ asset('assets/img/icon.png') }}" class="logo-icon margin-r-10">
                <img alt="" src="{{ asset('assets/img/logo-dark.png') }}" class="toggle-none hidden-xs">
              </h1>
            </a>
            <div class="left-nav-toggle" >
              <a  href="#" class="nav-collapse"><i class="fa fa-bars"></i></a>
            </div>
            <div class="left-nav-collapsed" >
              <a  href="#" class="nav-collapsed"><i class="fa fa-bars"></i></a>
            </div>
          </div>
          <div class="col">
            <ul class="list-inline top-right-nav">
              <li class="dropdown avtar-dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <img alt="" class="rounded-circle" src="{{ asset('assets/img/avtar-2.png') }}" width="30">
                {{ Auth::user() ? Auth::user()->name : '' }}
                </a>
                <ul class="dropdown-menu top-dropdown">
                  <li>
                    <a class="dropdown-item" href="{{ url('logout') }}"><i class="icon-logout"></i> Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- ============================================================== -->
    <!--                        Topbar End                              -->
    <!-- ============================================================== -->
    
    <!-- ============================================================== -->
    <!-- 						Navigation Start 						-->
    <!-- ============================================================== -->
    <div class="main-sidebar-nav default-navigation">
      <div class="nano">
        <div class="nano-content sidebar-nav">
          <ul class="metisMenu nav flex-column" id="menu">
            @include('_layouts.menu')
          </ul>
        </div>
      </div>
    </div>
    <!-- ============================================================== -->
    <!-- 						Navigation End	 						-->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- 						Content Start	 						-->
    <!-- ============================================================== -->
    <div class="row page-header">
      <div class="col-lg-6 align-self-center ">
        <h2>{{ isset($data) ? $data['_title'] ? $data['_title'] : '' : '' }}</h2>
        @yield('breadcrumbs')
      </div>
    </div>
    <section class="main-content">
      @yield('content')
      <footer class="footer">
        <span>Copyright &copy; 2021</span>
      </footer>
    </section>
    <!-- ============================================================== -->
    <!-- 						Content Send	 						-->
    <!-- ============================================================== -->
    <!-- Common Plugins -->
    <script src="{{ asset('assets/lib/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/lib/pace/pace.min.js') }}"></script>
    <script src="{{ asset('assets/lib/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/lib/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('assets/lib/nano-scroll/jquery.nanoscroller.min.js') }}"></script>
    <script src="{{ asset('assets/lib/metisMenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('loading/jquery.loading.min.js') }}"></script>
    
    @yield('scripts')
  </body>
</html>