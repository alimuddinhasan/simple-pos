<li class="nav-heading"><span>MAIN</span></li>
<li class="nav-item {{ isActiveUri('home') }}">
  <a class="nav-link" href="{{ url('home') }}">
    <i class="fa fa-home"></i> <span class="toggle-none">Home
  </a>
</li>
@permission('read-merchants|read-merchant-users')
<li class="nav-heading"><span>MERCHANT</span></li>
<li class="nav-item {{ isActiveUri(['merchant*']) }}">
  <a class="nav-link" href="{{ url('merchant') }}">
    <i class="fa fa-university"></i> <span class="toggle-none">Merchant
  </a>
</li>
@endpermission

@permission('read-sales')
<li class="nav-heading"><span>PENJUALAN</span></li>
<li class="nav-item">
  <a class="nav-link" href="{{ url('sale') }}">
    <i class="fa fa-print"></i> <span class="toggle-none">Transaksi
  </a>
</li>
@endpermission

@permission('read-categories|read-products')
<li class="nav-heading"><span>PRODUK</span></li>
<li class="nav-item {{ isActiveUri(['product*', 'category*']) }}">
  <a class="nav-link" href="javascript: void(0);" aria-expanded="false">
    <i class="fa fa-shopping-bag"></i> <span class="toggle-none">Produk <span class="fa arrow"></span></span>
  </a>
  <ul class="nav-second-level nav flex-column " aria-expanded="false">
    @permission('read-products')
    <li class="nav-item"><a class="nav-link" href="{{ url('product') }}">Produk</a></li>
    @endpermission
    @permission('read-categories')
    <li class="nav-item"><a class="nav-link" href="{{ url('category') }}">Kategori</a></li>
    @endpermission
  </ul>
</li>
@endpermission
@permission('read-customers')
<li class="nav-heading"><span>BISNIS</span></li>
@endpermission
@permission('read-customers')
<li class="nav-item {{ isActiveUri(['customer*']) }}">
  <a class="nav-link" href="{{ url('customer') }}">
    <i class="fa fa-users"></i> <span class="toggle-none">Pelanggan
  </a>
</li>
@endpermission
@permission('read-users|read-roles')
<li class="nav-heading"><span>PENGATURAN</span></li>
@endpermission

@permission('read-users')
<li class="nav-item {{ isActiveUri('user*') }}">
  <a class="nav-link" href="{{ url('user') }}">
    <i class="fa fa-user"></i> <span class="toggle-none">Users
  </a>
</li>
@endpermission

@permission('read-roles')
<li class="nav-item {{ isActiveUri('role*') }}">
  <a class="nav-link" href="{{ url('role') }}">
    <i class="fa fa-bolt"></i> <span class="toggle-none">Roles
  </a>
</li>
@endpermission
