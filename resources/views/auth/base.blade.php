
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>POS System</title>

	<!-- Common Plugins -->
	<link href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
	
	@yield('styles')

	<!-- Custom Css-->
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
			html,body{
					height: 100%;
			}
	</style>
</head>
<body class="bg-light">
	<div class="misc-wrapper">
		<div class="misc-content">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-5">
						@yield('content')
						<div class="text-center misc-footer">
							<p>&copy; {{ date('Y') }}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Common Plugins -->
	<script src="{{ asset('assets/lib/jquery/dist/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/lib/bootstrap/js/popper.min.js') }}"></script>
	<script src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/lib/pace/pace.min.js') }}"></script>
	<script src="{{ asset('assets/lib/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/lib/slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('assets/lib/nano-scroll/jquery.nanoscroller.min.js') }}"></script>
	<script src="{{ asset('assets/lib/metisMenu/metisMenu.min.js') }}"></script>
	<script src="{{ asset('assets/js/custom.js') }}"></script>

	@yield('scripts')

</body>
</html>
