@extends('auth.base')

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript">
  $(document).ready( function () {
    $("#form").validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: "required"
      },
      messages: {
        email: {
          required: "Masukkan email",
          email: "Email tidak valid"
        },
        password: "Masukkan password"
      },
      errorElement: "em",
      errorPlacement: function ( error, element ) {
        error.addClass( "help-block" );
        if ( element.prop( "type" ) === "checkbox" ) {
          error.insertAfter( element.parent( "label" ) );
        } else {
          error.insertAfter( element );
        }
      },
      highlight: function ( element, errorClass, validClass ) {
        $( element ).parents( ".form-group" ).addClass( "has-error" );
      },
      unhighlight: function (element, errorClass, validClass) {
        $( element ).parents( ".form-group" ).removeClass( "has-error" );
      }
    });
  });
</script>
@endsection

@section('content')
<div class="misc-header text-center">
	<img alt="" src="assets/img/icon.png" class="logo-icon margin-r-10">
	<img alt="" src="assets/img/logo-dark.png" class="toggle-none hidden-xs">
</div>
<div class="misc-box">
	@if (count($errors) > 0)
	@foreach ($errors->all() as $error)
	<div class="alert bg-danger alert-dismissible margin-b-10" role="alert"> 
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
		{!! $error !!}
	</div>
	@endforeach
	@endif
	<form role="form" id="form" action="{{ url('login') }}" method="post">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="email">Email</label>
			<div class="group-icon">
				<input id="email" type="text" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
				<span class="icon-user text-muted icon-input"></span>
			</div>
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<div class="group-icon">
				<input id="password" type="password" placeholder="Password" class="form-control" name="password">
				<span class="icon-lock text-muted icon-input"></span>
			</div>
		</div>
		<div class="clearfix">
			<div class="float-right">
				<button type="submit" class="btn btn-primary">Login</button>
			</div>
		</div>
	</form>
</div>
@endsection