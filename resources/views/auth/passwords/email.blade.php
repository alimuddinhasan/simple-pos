@extends('auth.base')

@section('content')
<div class="misc-header text-center">
  <img alt="" src="{{ asset('assets/img/icon.png') }}" class="logo-icon margin-r-10">
  <img alt="" src="{{ asset('assets/img/logo-dark.png') }}" class="toggle-none hidden-xs">
</div>
<div class="misc-box">
  <h3 class="text-center"><small>Lupa Password</small></h3>
  {{-- <div class="alert alert-info">
    <p class="">Masukkan email Anda, kami akan mengirimkan instruksi untuk reset password!</p>
  </div> --}}
  <form role="form">
    <div class="form-group group-icon">
      <input id="emailid" type="password" placeholder="Masukkan email" class="form-control">
      <span class="icon-envelope text-muted icon-input"></span>
    </div>
    <div class="clearfix">
      <a href="index.html" class="btn btn-block btn-primary">Reset Password</a>
    </div>
  </form>
</div>
@endsection
