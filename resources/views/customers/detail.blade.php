@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('customer') !!}
@endsection

@section('styles')
<style>
tr td{
  padding: 5px !important;
}
</style>
@endsection

@section('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<script type="text/javascript" src="{{ asset('assets/js/numeral.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/numeral.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lodash.js') }}"></script>
<script type="text/javascript">
$(".details").on('click', function(e) {
  e.preventDefault()
  var saleId = $(this).data('sales-id')
  $.ajax({
    type: 'GET',
    url: '{{ url('sale') }}/'+saleId+'/get',
    success: function (result) {
      $('#details-table > tbody').empty();;
      var items = result;
      _.each(items.details, function (v) {
        var content = `<td>${v.product.name}</td>
          <td>${v.qty} ${v.product.units}</td>
          <td>${numeral(v.unit_price).format('$0,0')}</td>
          <td>${numeral(v.total_price).format('$0,0')}</td>`
        $('#details-table > tbody:last-child').append(`<tr>${content}</tr>`);
      })

      $("#sub-total").html(numeral(items.sub_total).format('$0,0'))
      $("#shipping-fee").html(numeral(items.shipping_fee).format('$0,0'))
      $("#total").html(numeral(items.total).format('$0,0'))
      $("#weight-total").html(numeral(items.weight).format('0,0') + ' gram')
      $("#courier").html(items.courier ? items.courier.toUpperCase() : '')
      $("#courier-service").html(items.courier_services)
      
      $(".bs-example-modal-default").modal('toggle');
    }
  });
})
</script>
    
@endsection

@section('content')
<div class="row">
  <div class="col-md-4">
    <div class="widget white-bg">
      <div class="padding-20 text-center">
        <img alt="Profile Picture" width="140" class="rounded-circle mar-btm margin-b-10 box-shadow " src="{{ asset($data['customer']->gender === 'F' ? 'assets/img/avtar-1.png' : 'assets/img/avtar-2.png') }}">
          <p class=" font-500 margin-b-0">{{$data['customer']->name}}</p>
          <p class="text-sm margin-b-0">{{ $data['customer']->note ? $data['customer']->note : '-' }}</p>
      </div>
    </div>
    <div class="widget bg-light text-center">
      <h2 class="mv-0">Rp {{moneyFormatter(collect($data['customer']->sales->where('sale_status', '!=', 5)->where('payment_status', 2))->sum('total'))}}</h2>
      <div class="margin-b-0 text-muted">Total Pembayaran</div>
    </div>
    <div class="widget bg-light text-center">
      <h2 class="mv-0">{{collect($data['customer']->sales->where('sale_status', '!=', 5)->where('payment_status', 2))->map(function ($item, $key) {
        return $item->details;
      })->collapse()->sum('qty')}}</h2>
      <div class="margin-b-0 text-muted">Total Barang</div>
    </div>
    <div class="widget bg-light text-center">
      <h2 class="mv-0">{{collect($data['customer']->sales->where('sale_status', '!=', 5)->where('payment_status', 2))->count()}}</h2>
      <div class="margin-b-0 text-muted">Total Transaksi</div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
        <div class="tabs">
          <ul class="nav nav-tabs">
              <li class="nav-item" role="presentation"><a class="nav-link  active" href="#home" aria-controls="home" role="tab" data-toggle="tab">Biodata</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Riwayat Pembelian</a></li>
          </ul>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
              <small class="text-muted">Nama </small>
              <p>{{ $data['customer']->name }}</p> 
              <small class="text-muted">Jenis Kelamin </small>
              <p>{{ $data['customer']->gender ? $data['customer']->gender === 'M' ? 'Laki-laki' : 'Perempuan'  : '-' }}</p> 
              <small class="text-muted">Email</small>
              <p>{{ $data['customer']->email ? $data['customer']->email : '-' }}</p>
              <small class="text-muted">No. Telp / HP</small>
              <p>{{ $data['customer']->phone ? $data['customer']->phone : '-' }}</p>
              <small class="text-muted">Alamat</small>
              <p>
                {{ $data['customer']->address }} <br>
                {{ $data['customer']->subdistrict_name }}, {{ $data['customer']->city_name }} <br>
                {{ $data['customer']->province_name }}, {{ $data['customer']->postal_code }}
              </p>
              <small class="text-muted">Catatan</small>
              <p>{{ $data['customer']->note ? $data['customer']->note : '-' }}</p>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
              @if($data['customer']->sales->count() <= 0)
              <p>Belum ada transaksi</p>
              @else
              <div class="widget white-bg">
                <ul class="comments-list list-unstyled clearfix">
                  @foreach ($data['customer']->sales as $d)
                  <li class="clearfix">
                    <div class="content">
                      <div class="comments-header">
                        <ul class="list-inline row pull-right">
                          <li class="nav-item"><a class="nav-link details" href="#" data-sales-id="{{$d->id}}" ><i class="fa fa-search text-primary"></i> Detail</a></li>
                        </ul>
                        <strong>#{{$d->invoice_no}}</strong>
                        <small class="text-muted">{{$d->invoice_date->format('d M Y')}}</small>
                        <hr>
                      </div>
                      <div class="row">
                        <div class="col-md-4 col-xs-6 b-r text-center"> <small class="text-muted">Sub Total</small>
                          <p>Rp {{ moneyFormatter($d->sub_total) }}</p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r text-center"> <small class="text-muted">Biaya Kirim</small>
                          <p>Rp {{ moneyFormatter($d->shipping_fee) }}</p>
                        </div>
                        <div class="col-md-4 col-xs-6 b-r text-center"> <small class="text-muted">Total</small>
                          <p>Rp {{ moneyFormatter($d->total) }}</p>
                        </div>
                      </div>
                      <div class="text-center">
                        @if($d->payment_status == 1)
                          <span class="label label-warning"><i class="fa fa-money"></i> Pending</span>
                        @elseif($d->payment_status == 2)
                          <span class="label label-success"><i class="fa fa-money"></i> Lunas</span>
                        @elseif($d->payment_status == 3)
                          <span class="label label-danger"><i class="fa fa-money"></i> Canceled</span>
                        @endif
                
                        @if($d->sale_status == 1)
                          <span class="label label-warning"><i class="fa fa-send"></i> Pending</span>
                        @elseif($d->sale_status == 2)
                          <span class="label label-default"><i class="fa fa-send"></i> On Process</span>
                        @elseif($d->sale_status == 3)
                          <span class="label label-default"><i class="fa fa-send"></i> On Delivery</span>
                        @elseif($d->sale_status == 4)
                          <span class="label label-success"><i class="fa fa-send"></i> Selesai</span>
                        @elseif($d->sale_status == 5)
                          <span class="label label-danger"><i class="fa fa-send"></i> Dibatalkan</span>
                        @endif
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-default" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times"></span></button>
          <h5 class="modal-title" id="myLargeModalLabel">Details</h5>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table" id="details-table">
                <thead>
                  <tr>
                    <td>Nama Produk</td>
                    <td>Jumlah</td>
                    <td>Harga Satuan</td>
                    <td>Total</td>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-4">
                  <small class="text-muted">Berat Total </small>
                  <p id="weight-total" style="margin-bottom: 0;"></p> 
                  
                </div>
                <div class="col-md-4">
                  <small class="text-muted">Kurir </small>
                  <p id="courier" style="margin-bottom: 0;"></p> 
                  
                </div>
                <div class="col-md-4">
                  <small class="text-muted">Jenis Layanan </small>
                  <p id="courier-service" style="margin-bottom: 0;"></p> 
                  
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <table class="table table-striped text-right pull-right">
                <tbody>
                  <tr>
                    <td><strong>Sub Total :</strong></td>
                    <td id="sub-total"></td>
                  </tr>
                  <tr>
                    <td><strong>Ongkos Kirim :</strong></td>
                    <td id="shipping-fee"></td>
                  </tr>
                  <tr>
                    <td><strong>TOTAL :</strong></td>
                    <td id="total"></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection