@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('customer') !!}
@endsection

@section('styles')
<link href="{{ asset('select2/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('select2/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<style>
.select2-container{ width: 100% !important; }
</style>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
@endsection

@section('content')
@php
  $action = url('customer');
  $patch_field = '';
  if (isset($data['customer'])) {
    $action = url('customer/' . $data['customer']->id);
    $patch_field = method_field('PATCH');
  }
@endphp
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        {{ isset($data['customer']) ? 'Edit Pelanggan' : 'Tambah Pelanggan' }}
      </div>
      <div class="card-body">
        <form id="form" method="post" class="form-horizontal" action="{{ $action }}" autocomplete="off">
          {{ csrf_field() }}
          {{ $patch_field }}
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="name">Name<span style="color: #f4516c;">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" 
                @if(!empty($data['customer']))
                value="{{ $data['customer']->name }}" 
                @endif/>
              </div>
              <div class="form-group">
                <label>Jenis Kelamin</label>
                <select name="gender" id="gender" class="form-control m-b">
                  <option value="">-- Choose one --</option>
                  <option value="M"
                    @if(!empty($data['customer']))
                    @if($data['customer']->gender === 'M')
                    selected 
                    @endif
                    @endif>
                    Laki-laki
                  </option>
                  <option value="F"
                    @if(!empty($data['customer']))
                    @if($data['customer']->gender === 'F')
                    selected 
                    @endif
                    @endif>
                    Perempuan
                  </option>
                </select>
              </div>
              <div class="form-group">
                <label for="phone">Telepon / HP</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Telepon / HP" 
                @if(!empty($data['customer']))
                value="{{ $data['customer']->phone }}" 
                @endif/>
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Email" 
                @if(!empty($data['customer']))
                value="{{ $data['customer']->email }}" 
                @endif/>
              </div>
              <div class="form-group">
                <label for="note">Catatan</label>
                <textarea name="note" id="note" style="height: 75px;" class="form-control">@if(!empty($data['customer'])){{ $data['customer']->note }}@endif</textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="address">Alamat</label>
                <textarea name="address" id="address" style="height: 75px;" class="form-control">@if(!empty($data['customer'])){{ $data['customer']->address }}@endif</textarea>
              </div>
              <div class="form-group">
                <label for="postal_code">Kode Pos</label>
                <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Kode Pos" 
                @if(!empty($data['customer']))
                value="{{ $data['customer']->postal_code }}" 
                @endif/>
              </div>
            </div>
          </div>
          <div class="checkbox checkbox-primary">
            <input id="active" name="active" type="checkbox" value="true"
              @if(!empty($data['customer']))
              @if($data['customer']->active)
              checked 
              @endif
              @else
              checked 
              @endif>
            <label for="active"> Active </label>
          </div>
          <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>          
          <div class="form-group">
            <a href="{{ url('customer') }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection