@extends('_layouts.base')

@section('content')
<div class="row">
  <div class="col-md-12">
    <h4>Not Allowed!</h4>
    <p>You're not allowed to access this page. </p>
  </div>
</div>
@endsection

