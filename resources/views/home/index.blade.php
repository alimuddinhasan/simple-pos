@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('home') !!}
@endsection

@section('styles')
<link href="{{ asset('assets/lib/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
<link href="{{ asset('select2/select2.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<meta name="purchase-id" content="{{ !empty($data['purchase']) ? $data['purchase']->id : '' }}">
<script type="text/javascript" src="{{ asset('select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap-daterangepicker/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/numeral.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/numeral.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lodash.js') }}"></script>
<script type="text/javascript">
var startDate = moment().format('YYYY-MM-DD')
var endDate = moment().format('YYYY-MM-DD')

$(document).ready(function () {
  refreshDashboard()
})

$("#received_date").daterangepicker({
  showDropdowns: true,
  alwaysShowCalendars: true,
  showCustomRangeLabel: false,
  locale: {
    format: 'DD-MM-YYYY'
  },
  ranges: {
    'Hari Ini': [moment(), moment()],
    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
    '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
    'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  }
}).on('apply.daterangepicker', function (ev, picker) {
  startDate = picker.startDate.format('YYYY-MM-DD')
  endDate = picker.endDate.format('YYYY-MM-DD')
  refreshDashboard()
});

function refreshDashboard() {
  $.ajax({
    type: 'GET',
    url: '{{ url('allSales') }}',
    data: {
      start: startDate,
      end: endDate
    },
    success: function (result) {
      var itemSold = _.sumBy(_.flatMap(result, ({ details }) => details), 'qty')
      var income = _.sumBy(result, 'total')
      var profit = income - (_.sumBy(result, 'total_cost') + _.sumBy(result, 'shipping_fee') )
      $('#total-transaction').html(result.length)
      $('#item-sold').html(itemSold)
      $("#income").html(numeral(income).format('$0,0'))
      $("#profit").html(numeral(profit).format('$0,0'))
    }
  })
}
</script>
@endsection

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <div class="input-group m-b">
        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
        <input type="text" name="received_date" id="received_date" class="form-control" />
      </div>
    </div>
  </div>
  <div class="col-md-6"></div>
  <div class="col-md-6">
    <div class="widget card-info">
      <div class="box bg-info text-center">
        <h1 class="font-light text-white" id="income">Rp 0</h1>
        <h6 class="text-white">Total Penjualan</h6>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="widget card-info">
      <div class="box bg-info text-center">
        <h1 class="font-light text-white" id="profit">Rp 0</h1>
        <h6 class="text-white">Total Laba</h6>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="widget card-warning">
      <div class="box bg-warning text-center">
        <h1 class="font-light text-white" id="total-transaction">0</h1>
        <h6 class="text-white">Total Transaksi</h6>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="widget card-warning">
      <div class="box bg-warning text-center">
        <h1 class="font-light text-white" id="item-sold">0</h1>
        <h6 class="text-white">Barang Terjual</h6>
      </div>
    </div>
  </div>
</div>
@endsection