@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('merchant') !!}
@endsection

@section('styles')
<style>
tr td{
  padding: 5px !important;
}
</style>
@endsection

@section('content')
<div class="row">
  <div class="col-md-4">
    <div class='widget white-bg clearfix'>
      <small class="text-muted">Nama Bisnis </small>
      <p>{{ $data['merchant']->name }}</p> 
      <small class="text-muted">No. HP </small>
      <p>{{ $data['merchant']->phone }}</p> 
      <small class="text-muted">Email</small>
      <p>{{ $data['merchant']->email }}</p>
      <small class="text-muted">Subscription</small>
      <p>
        @php
          $subs = $data['merchant']->subscription('main');
        @endphp
        @if ($subs->plan->name === 'Basic')
        <span class="label label-warning"> Free</span>
        @elseif ($subs->plan->name === 'Pro Bulanan')
          <span class="label label-success"> Pro Bulanan</span>
        @elseif ($subs->plan->name === 'Pro Tahunan')
          <span class="label label-success"> Pro Tahunan</span>
        @else
          <span class="label label-default"> None</span>
        @endif
      </p>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
        <div class="btn-group">
            <ul class="list-inline">
              <li class="dropdown">
                <a class="btn text-primary" data-toggle="dropdown" href="#"> <i class="fa fa-ellipsis-v"></i></a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="dropdown-item" href="{{ url('merchantUser?id='.$data['merchant']->id) }}"><i class="icon-user"></i> Merchant User</a>
                  </li>
                  {{-- <li class="dropdown-divider"></li> --}}
                  <li>
                    <a class="dropdown-item text-danger" href="#"><i class="icon-trash"></i> Hapus Merchant</a>
                  </li>
                </ul>
              </li>
            </ul>
        </div>
        <a href="#" class="btn" >Upgrade</a>
        <a href="#" class="btn" >Perbarui</a>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="tabs">
          <ul class="nav nav-tabs">
              <li class="nav-item" role="presentation"><a class="nav-link  active" href="#about" aria-controls="about" role="tab" data-toggle="tab">Tentang Merchant</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link" href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab">Subscription</a></li>
          </ul>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="about">
              <small class="text-muted">Alamat </small>
              <p>{{ $data['merchant']->address ? $data['merchant']->address : '-' }}</p> 
              <small class="text-muted">Provinsi </small>
              <p>{{ $data['merchant']->province_name ? $data['merchant']->province_name : '-' }}</p> 
              <small class="text-muted">Kota </small>
              <p>{{ $data['merchant']->city_name ? $data['merchant']->city_name : '-' }}</p> 
              <small class="text-muted">Kecamatan </small>
              <p>{{ $data['merchant']->subdistrict_name ? $data['merchant']->subdistrict_name : '-' }}</p> 
            </div>
            <div role="tabpanel" class="tab-pane" id="subscription">
              <small class="text-muted">Plan </small>
              <p>{{ $subs->plan->name }}</p>
              <small class="text-muted">Harga </small>
              <p>Rp. {{ moneyFormatter($subs->plan->price) }}</p>
              <small class="text-muted">Mulai </small>
              <p>{{ $subs->starts_at->format('d-m-Y H:i:s') }}</p>
              <small class="text-muted">Selesai </small>
              <p>{{ $subs->ends_at->format('d-m-Y H:i:s') }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection