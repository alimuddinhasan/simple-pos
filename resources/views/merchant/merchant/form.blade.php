@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('merchant') !!}
@endsection

@section('styles')
<link href="{{ asset('select2/select2.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $("#form").validate({
    rules: {
      name: "required",
      phone: {
        digits: true
      },
      email: {
        required: true,
        remote: "/user/check",
        email: true
      }
    },
    messages: {
      name: "Nama bisnis tidak boleh kosong",
      phone: {
        digits: "Nomor hp tidak valid"
      },
      email: {
        required: "Email tidak boleh kosong",
        remote: "Email sudah terdaftar",
        email: "Email tidak valid"
      }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".form-group" ).addClass( "has-error" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).removeClass( "has-error" );
    }
  });
});
</script>
@endsection

@section('content')
@php
  $action = url('merchant');
  $patch_field = '';
  if (isset($data['merchant'])) {
    $action = url('merchant/' . $data['merchant']->id);
    $patch_field = method_field('PATCH');
  }
@endphp
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header card-default">
        {{ isset($data['merchant']) ? 'Edit Merchant' : 'Create Merchant' }}
      </div>
      <div class="card-body">
        <form id="form" method="post" class="form-horizontal" action="{{ $action }}" autocomplete="off">
          {{ csrf_field() }}
          {{ $patch_field }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="name">Nama Bisnis<span style="color: #f4516c;">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Nama Bisnis" 
                @if(!empty($data['merchant']))
                value="{{ $data['merchant']->name }}" 
                @endif/>
              </div>
              <div class="form-group">
                <label for="phone">No. HP<span style="color: #f4516c;">*</span></label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="No. HP" 
                @if(!empty($data['merchant']))
                value="{{ $data['merchant']->phone }}"
                @endif/>
              </div>
              <div class="form-group">
                <label for="email">Email<span style="color: #f4516c;">*</span></label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Email" 
                @if(!empty($data['merchant']))
                value="{{ $data['merchant']->email }}" disabled
                @endif/>
              </div>
            </div>
          </div>
          <div class="checkbox checkbox-primary">
            <input id="active" name="active" type="checkbox" value="true"
              @if(!empty($data['merchant']))
              @if($data['merchant']->active)
              checked 
              @endif
              @else
              checked 
              @endif>
            <label for="active"> Active </label>
          </div>
          <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>          
          <div class="form-group">
            <a href="{{ url('merchant') }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection