@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('user') !!}
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript">
  $.validator.addMethod(
    "regex",
    function(value, element, regexp) {
      var re = new RegExp(regexp);
      return this.optional(element) || re.test(value);
    },
    "Please check your input."
  );
  $(document).ready( function () {
    $("#form").validate({
      rules: {
        password: {
          required: true,
          regex: "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})"
        },
        confirm_password: {
          equalTo: "#password"
        }
      },
      messages: {
        password: {
          required: "Masukkan password",
          regex: "Password harus 8 karakter atau lebih serta mengandung huruf besar, huruf kecil, dan angka."
        },
        confirm_password: {
          equalTo: "Password tidak sama"
        }
      },
      errorElement: "em",
      errorPlacement: function ( error, element ) {
        error.addClass( "help-block" );
        if ( element.prop( "type" ) === "checkbox" ) {
          error.insertAfter( element.parent( "label" ) );
        } else {
          error.insertAfter( element );
        }
      },
      highlight: function ( element, errorClass, validClass ) {
        $( element ).parents( ".form-group" ).addClass( "has-error" );
      },
      unhighlight: function (element, errorClass, validClass) {
        $( element ).parents( ".form-group" ).removeClass( "has-error" );
      }
    });
  });
</script>
@endsection

@section('content')
@php
  $action = url('merchantUser/' . $data['user']->id) . '/changepassword';
@endphp
<div class="row">
  <div class="col-md-4">
    <div class='widget white-bg friends-group clearfix'>
      <small class="text-muted">Nama Bisnis </small>
      <p>{{ $data['merchant']->name }}</p> 
      <small class="text-muted">Name </small>
      <p>{{ $data['user']->name }}</p>
      <small class="text-muted">Email </small>
      <p>{{ $data['user']->email }}</p>
      <small class="text-muted">Roles </small>
      @foreach($data['user']->roles as $d)
        <p><span class="label label-success">{{$d->display_name}}</span></p>
      @endforeach
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header card-default">
        Reset Password
      </div>
      <div class="card-body">
        <form id="form" method="post" class="form-horizontal" action="{{ $action }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="password">Password<span style="color: #f4516c;">*</span></label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
          </div>
          <div class="form-group">
            <label for="confirm_password">Confirm Password<span style="color: #f4516c;">*</span></label>
            <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" />
          </div>
          <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>
          <div class="form-group">
            <a href="{{ url('merchantUser?id='.$data['merchant']->id) }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection