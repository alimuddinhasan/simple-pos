@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('merchantUser') !!}
@endsection

@section('styles')
<link href="{{ asset('select2/select2.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $("#form").validate({
    rules: {
      name: "required",
      email: {
        required: true,
        email: true,
        remote: "/user/check"
      },
      role: "required"
    },
    messages: {
      name: "Masukkan nama",
      email: {
        required: "Masukkan email",
        email: "Email tidak valid",
        remote: "Email sudah terdaftar"
      },
      role: "Pilih role user"
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".form-group" ).addClass( "has-error" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).removeClass( "has-error" );
    }
  });
});
</script>
@endsection

@section('content')
@php
  $action = url('merchantUser');
  $patch_field = '';
  if (isset($data['merchantUser'])) {
    $action = url('merchantUser/' . $data['merchantUser']->id);
    $patch_field = method_field('PATCH');
  }
@endphp
<div class="row">
  <div class="col-md-4">
    <div class='widget white-bg friends-group clearfix'>
      <small class="text-muted">Nama Bisnis </small>
      <p>{{ $data['merchant']->name }}</p> 
      <small class="text-muted">No. HP </small>
      <p>{{ $data['merchant']->phone }}</p> 
      <small class="text-muted">Email</small>
      <p>{{ $data['merchant']->email }}</p>
      <small class="text-muted">Subscription</small>
      <p>
        @php
          $subs = $data['merchant']->subscription('main');
        @endphp
        @if ($subs->plan->name === 'Free')
          <span class="label label-warning"> Free</span>
        @elseif ($subs->plan->name === 'Pro')
          <span class="label label-success"> Pro</span>
        @else
          <span class="label label-default"> None</span>
        @endif
      </p>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card">
      <div class="card-header card-default">
        {{ isset($data['merchantUser']) ? 'Edit User' : 'Tambah User' }}
      </div>
      <div class="card-body">
        <form id="form" method="post" class="form-horizontal" action="{{ $action }}">
          <input type="text" hidden name="merchant_id" value="{{ $data['merchant']->id }}" />
          {{ csrf_field() }}
          {{ $patch_field }}
          <div class="form-group">
            <label for="name">Nama<span style="color: #f4516c;">*</span></label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" 
            @if(!empty($data['merchantUser']))
            value="{{ $data['merchantUser']->name }}" 
            @endif/>
          </div>
          <div class="form-group">
            <label for="email">Email<span style="color: #f4516c;">*</span></label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Email" 
            @if(!empty($data['merchantUser']))
            value="{{ $data['merchantUser']->email }}" disabled
            @endif/>
          </div>
          <div class="form-group">
            <label>Role<span style="color: #f4516c;">*</span></label>
            <select name="role" id="role" class="form-control m-b">
              <option value="">-- Choose one --</option>
              @foreach($datalist['roles'] as $d)
              <option value="{{ $d->id }}"
                @if(!empty($data['merchantUser']))
                @if(isset($data['merchantUser']->roles[0]) ? $data['merchantUser']->roles[0]->id === $d->id : false)
                selected 
                @endif
                @endif>
                {{ $d->display_name }}
              </option>
              @endforeach
            </select>
          </div>
          
          <div class="checkbox checkbox-primary">
            <input id="active" name="active" type="checkbox" value="true"
              @if(!empty($data['merchantUser']))
              @if($data['merchantUser']->active)
              checked 
              @endif
              @else
              checked 
              @endif>
            <label for="active"> Active </label>
          </div>
          <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>          
          <div class="form-group">
            <a href="{{ url('merchantUser?id='.$data['merchant']->id) }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection