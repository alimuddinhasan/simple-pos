@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('category') !!}
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $("#form").validate({
    rules: {
      name: "required"
    },
    messages: {
      name: "Masukkan nama merchant"
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".form-group" ).addClass( "has-error" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).removeClass( "has-error" );
    }
  });
});
</script>
@endsection

@section('content')
@php
  $action = url('category');
  $patch_field = '';
  if (isset($data['category'])) {
    $action = url('category/' . $data['category']->id);
    $patch_field = method_field('PATCH');
  }
@endphp
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header card-default">
        {{ isset($data['category']) ? 'Ubah Kategori' : 'Buat Kategori' }}
      </div>
      <div class="card-body">
        <form id="form" method="post" class="form-horizontal" action="{{ $action }}" autocomplete="off">
          {{ csrf_field() }}
          {{ $patch_field }}
          <div class="form-group">
            <label for="name">Nama<span style="color: #f4516c;">*</span></label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" 
            @if(!empty($data['category']))
            value="{{ $data['category']->name }}" 
            @endif/>
          </div>
          <div class="form-group">
            <label for="description">Keterangan</label>
            <textarea name="description" id="description" style="height: 75px;" class="form-control">@if(!empty($data['category'])){{ $data['category']->description }}@endif</textarea>
          </div>
          <div class="checkbox checkbox-primary">
            <input id="active" name="active" type="checkbox" value="true"
              @if(!empty($data['category']))
              @if($data['category']->active)
              checked 
              @endif
              @else
              checked 
              @endif>
            <label for="active"> Aktif </label>
          </div>
          <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>          
          <div class="form-group">
            <a href="{{ url('category') }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection