@extends('_layouts.base')

@section('styles')
<link href="{{ asset('assets/lib/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/lib/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('scripts')
<script src="{{ asset('assets/lib/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lib/datatables/dataTables.responsive.min.js') }}"></script>
<script>
  $('#datatable1').on('click', 'a.delete-btn' , function(e) {
    e.preventDefault();
    var del = confirm('Yakin hapus data?');
    if (del == true) {
      window.location = $(this).attr('href');
    }
  });
  $(document).ready(function () {
    $('#datatable1').dataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('category.fetch') !!}',
      columns: [
        { data: 'name', name: 'name' },
        { data: 'active', name: 'active', className: 'text-center' },
        { data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false }
      ],
      autoWidth: false
    });
  });
</script>
@endsection

@section('breadcrumbs')
{!! Breadcrumbs::render('category') !!}
@endsection

@section('content')
<div class="row">

  @if(session()->has('alert'))
  <div class="col-md-12">
    <div class="alert bg-{{ session()->get('alert')['type'] }} alert-dismissible margin-b-10" role="alert"> 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
      {!! session()->get('alert')['message'] !!}
    </div>
  </div>
  @endif

  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        <div class="float-right mt-10">
          <a href="{{ url('category/create') }}" class="btn btn-primary btn-icon btn-rounded box-shadow"><i class="fa fa-plus"></i> Tambah Kategori</a>
        </div>
        Kategori
      </div>
      <div class="card-body">
        <table id="datatable1" class="table table-striped dt-responsive nowrap table-hover">
          <thead>
            <tr>
              <th class="text-center">
                <strong>Nama Kategori</strong>
              </th>
              <th class="text-center" style="width:100px;">
                <strong>Status</strong>
              </th>
              <th class="text-center" style="width:85px;">
                <strong>Action</strong>
              </th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection