@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('product') !!}
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready( function () {
  var otherUnits = $("#other_units");
  otherUnits.hide(); 
  showHideOtherValue();
  $("input[name='units']").change(showHideOtherValue); 

  function showHideOtherValue() {
    var radioVal = $("input[name='units']:checked").val();
    if (radioVal === 'lainnya') {
      otherUnits.show();
    } else {
      otherUnits.hide();
    }
  }

  $("#form").validate({
    rules: {
      name: "required",
      other_units: {
        required: "#lainnya:checked"
      },
      category_id: "required",
      weight: "required",
      unit_price: "required"
    },
    messages: {
      name: "Masukkan nama produk",
      other_units: "Masukan satuan",
      category_id: "Pilih kategori",
      weight: "Masukkan berat",
      unit_price: "Masukkan harga"
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".form-group" ).addClass( "has-error" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).removeClass( "has-error" );
    }
  });
});
</script>
@endsection

@section('content')
@php
  $action = url('product');
  $patch_field = '';
  if (isset($data['product'])) {
    $action = url('product/' . $data['product']->id);
    $patch_field = method_field('PATCH');
  }
@endphp
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        {{ isset($data['product']) ? 'Ubah Produk' : 'Buat Produk' }}
      </div>
      <div class="card-body">
        @if(count($datalist['categories']) <= 0)
          <p>Anda belum memiliki kategori, silahkan <a href="{{ url('category') }}">Buat Kategori</a> terlebih dahulu</p>
        @else
        <form id="form" method="post" class="form-horizontal" action="{{ $action }}" enctype="multipart/form-data" autocomplete="off">
          {{ csrf_field() }}
          {{ $patch_field }}
          <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <label for="name">Nama Produk<span style="color: #f4516c;">*</span></label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Nama Produk" 
                @if(!empty($data['product']))
                value="{{ $data['product']->name }}" 
                @endif/>
              </div>
              <div class="form-group">
                <label for="description">Keterangan</label>
                <textarea name="description" id="description" style="height: 75px;" class="form-control">@if(!empty($data['product'])){{ $data['product']->description }}@endif</textarea>
              </div>
              <div class="form-group">
                <label>Kategori<span style="color: #f4516c;">*</span></label>
                <select name="category_id" id="category_id" class="form-control m-b">
                  <option value="">-- Choose one --</option>
                  @foreach($datalist['categories'] as $d)
                  <option value="{{ $d->id }}"
                    @if(!empty($data['product']))
                    @if($data['product']->category_id === $d->id)
                    selected 
                    @endif
                    @endif>
                    {{ $d->name }}
                  </option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="weight">Berat (gram)<span style="color: #f4516c;">*</span></label>
                <input type="text" class="form-control" id="weight" name="weight" placeholder="Berat" 
                @if(!empty($data['product']))
                value="{{ $data['product']->weight }}" 
                @endif/>
              </div>
              <div class="form-group">
                <label for="unit_cost">Harga Beli<span style="color: #f4516c;">*</span></label>
                <input type="text" class="form-control" id="unit_cost" name="unit_cost" placeholder="Harga Beli Per Unit" 
                @if(!empty($data['product']))
                value="{{ $data['product']->unit_cost }}" 
                @endif/>
              </div>
              <div class="form-group">
                <label for="unit_price">Harga Jual<span style="color: #f4516c;">*</span></label>
                <input type="text" class="form-control" id="unit_price" name="unit_price" placeholder="Harga Jual Per Unit" 
                @if(!empty($data['product']))
                value="{{ $data['product']->unit_price }}" 
                @endif/>
              </div>
              <div class="form-group">
                <label>Satuan</label>
                <div class="form-inline">
                  <div class="radio radio-primary">
                    <input type="radio" name="units" id="pcs" value="pcs" 
                    {{ empty($data['product']) ? 'checked' : $data['product']->units === 'pcs' ? 'checked' : ''}}>
                    <label for="pcs"> pcs </label>
                  </div>
                  <div class="radio radio-primary">
                    <input type="radio" name="units" id="kg" value="kg"
                    {{ empty($data['product']) ? '' : $data['product']->units === 'kg' ? 'checked' : ''}}>
                    <label for="kg"> kg </label>
                  </div>
                  <div class="radio radio-primary">
                    <input type="radio" name="units" id="box" value="box"
                    {{ empty($data['product']) ? '' : $data['product']->units === 'box' ? 'checked' : ''}}>
                    <label for="box"> box </label>
                  </div>
                  <div class="radio radio-primary">
                    <input type="radio" name="units" id="lainnya" value="lainnya"
                    {{ empty($data['product']) ? '' : !in_array($data['product']->units, ['pcs', 'kg', 'box']) ? 'checked' : ''}}>
                    <label for="lainnya"> lainnya </label>
                  </div>
                  <div style="padding-left:20px; width:60px;">
                    <input type="text" style=" width:60px;" class="form-control" id="other_units" name="other_units"
                    value="{{ empty($data['product']) ? '' : !in_array($data['product']->units, ['pcs', 'kg', 'box']) ? $data['product']->units : ''}}" />
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Gambar</label>
                @php
                  $img = null;
                  if (!empty($data['product'])) {
                    $img = $data['product']->firstMedia('thumbnail');
                  }
                @endphp
                <div class="{{ $img ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
                  <div class="fileinput-preview" data-trigger="fileinput" style="width: 200px; min-height:150px;">
                    @if($img)
                      <img class="img-fluid" src="{{ $img->getUrl() }}">
                    @endif
                  </div>
                  <span class="btn btn-primary  btn-file">
                    <span class="fileinput-new">Upload</span>
                    <span class="fileinput-exists">Ganti</span>
                    <input type="file" id="thumbnail" name="thumbnail" accept="image/*">
                  </span>
                  <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
                </div>
              </div>
            </div>
          </div>
          <div class="checkbox checkbox-primary">
            <input id="active" name="active" type="checkbox" value="true"
              @if(!empty($data['product']))
              @if($data['product']->active)
              checked 
              @endif
              @else
              checked 
              @endif>
            <label for="active"> Active </label>
          </div>
          <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>          
          <div class="form-group">
            <a href="{{ url('product') }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection