@extends('_layouts.base')

@section('styles')
<link href="{{ asset('assets/lib/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/lib/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('scripts')
<script src="{{ asset('assets/lib/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lib/datatables/dataTables.responsive.min.js') }}"></script>
<script>
  $('#table').on('click', 'a.delete-btn' , function(e) {
    e.preventDefault();
    var del = confirm('Are you sure delete this data?');
    if (del == true) {
      window.location = $(this).attr('href');
    }
  });
  $(document).ready(function () {
    $('#table').dataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('product.fetch') !!}',
      columns: [
        { data: 'thumbnail', name: 'thumbnail', className: 'text-center', orderable: false, searchable: false },
        { data: 'name', name: 'name' },
        { data: 'unit_price', name: 'unit_price', className: 'text-right' },
        { data: 'active', name: 'active', className: 'text-center' },
        { data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false }
      ],
      autoWidth: false,
      order: [[1, 'asc']]
    });
  });
</script>
@endsection

@section('breadcrumbs')
{!! Breadcrumbs::render('product') !!}
@endsection

@section('content')
<div class="row">

  @if(session()->has('alert'))
  <div class="col-md-12">
    <div class="alert bg-{{ session()->get('alert')['type'] }} alert-dismissible margin-b-10" role="alert"> 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
      {!! session()->get('alert')['message'] !!}
    </div>
  </div>
  @endif

  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        <div class="float-right mt-10">
          <a href="{{ url('product/create') }}" class="btn btn-primary btn-icon btn-rounded box-shadow"><i class="fa fa-plus"></i> Tambah Produk</a>
        </div>
        Produk
        {{-- <p class="text-muted">Lorem Ipsum is simply dummy text of the <code>printing and typesetting</code> industry.</p> --}}
      </div>
      <div class="card-body">
        <table id="table" class="table table-striped dt-responsive nowrap table-hover">
          <thead>
            <tr>
              <th style="width: 50px;"></th>
              <th class="text-center">
                <strong>Nama Produk</strong>
              </th>
              <th class="text-center" style="width: 120px;">
                <strong>Harga</strong>
              </th>
              <th class="text-center" style="width: 100px;">
                <strong>Status</strong>
              </th>
              <th class="text-center" style="width: 85px;">
                <strong>Action</strong>
              </th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection