@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('sale') !!}
@endsection

@section('styles')
<style>
tr td{
  padding: 10px !important;
}
</style>
@endsection

@section('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}"> 
<script type="text/javascript" src="{{ asset('assets/js/lodash.js') }}"></script>
<script type="text/javascript">
$('#cancel-btn').on('click', function(e) {
  e.preventDefault();
  var del = confirm('Anda yakin membatalkan transaksi ini?');
  if (del == true) {
    window.location = $(this).attr('href');
  }
});
$("#history").on('click', function(e) {
  e.preventDefault()
  var waybillNo = '{{ $data['sale']->waybill_no }}'
  var courier = '{{ $data['sale']->courier }}'
  if (waybillNo && courier) {
    $.ajax({
      type: 'POST',
      url: '{{ route('rajaongkir.waybill') }}',
      data: {
        _token: $('meta[name=csrf-token]').attr('content'),
        waybill: waybillNo,
        courier: courier
      },
      success: function (result) {
        $('#status-table').hide()
        if (result.status.code === 200) {
          $('#status-table > tbody').empty()
          var statuses = result.result.manifest;
          console.log(statuses)
          _.each(statuses, function (v) {
            var content = `<td>${v.manifest_date} ${v.manifest_time}</td>
              <td>${v.manifest_description}</td>`
            $('#status-table > tbody:last-child').append(`<tr>${content}</tr>`);
          })
          $('#status-table').show()
        } else {
          $('.modal-history .modal-body').html('<p>'+result.status.description+'</p>')
        }
        $(".modal-history").modal('show');
      }
    });
  } else {
    alert('Nomor resi belum diinputkan')
  }
})

$("#btn-edit-payment-status").on('click', function (e) {
  e.preventDefault()
  $.ajax({
    type: 'GET',
    url: '{{ url('sale/' . $data['sale']->id . '/get' ) }}',
    success: function (result) {
      console.log(result)
      $('#payment_status').empty()
      var status = result.payment_status
      var options = `
        <option value="1" ${ status == 1 ? 'selected' : '' } >Pending</option>
        <option value="2" ${ status == 2 ? 'selected' : '' } >Lunas</option>
      `
      $('#payment_status').append(options)
      $("#modal-payment-status").modal('toggle')
      console.log('jancuks')
    }
  })
})
$("#btn-edit-sale-status").
  on('click', function (e) {
    e.preventDefault()
    console.log('cuks')
    $.ajax({
      type: 'GET',
      url: '{{ url('sale/' . $data['sale']->id . '/get' ) }}',
      success: function (result) {
        $('#sale_status').empty()
        var status = result.sale_status
        var options = `
          <option value="1" ${ status == 1 ? 'selected' : '' } >Pending</option>
          <option value="2" ${ status == 2 ? 'selected' : '' } >On Process</option>
          <option value="3" ${ status == 3 ? 'selected' : '' } >On Delivery</option>
          <option value="4" ${ status == 4 ? 'selected' : '' } >Selesai</option>
        `
        $('#sale_status').append(options)
        $("#modal-sale-status").modal('toggle')
      }
    })
  })

$("#form-payment-status").on('submit', function (e) {
  e.preventDefault();
  var status = $("#payment_status").find(":selected").val();
  $.ajax({
    type: 'PATCH',
    url: '{{ url('sale/' . $data['sale']->id . '/updateStatus' ) }}',
    data: {
      _token: $('meta[name=csrf-token]').attr('content'),
      payment_status: status
    },
    success: function (result) {
      console.log(result)
      var content = '';
      if (status == 1) {
        content = '<span class="label label-warning"> Pending</span>'
      } else if (status == 2) {
        content = '<span class="label label-success"> Lunas</span>'
      }
      $('#payment-status').html(content)
      $("#modal-payment-status").modal('hide');
    }
  })
})
$("#form-sale-status").on('submit', function (e) {
  e.preventDefault();
  var status = $("#sale_status").find(":selected").val();
  $.ajax({
    type: 'PATCH',
    url: '{{ url('sale/' . $data['sale']->id . '/updateStatus' ) }}',
    data: {
      _token: $('meta[name=csrf-token]').attr('content'),
      sale_status: status
    },
    success: function (result) {
      console.log(result)
      var content = '';
      if (status == 1) {
        content = '<span class="label label-warning"> Pending</span>'
      } else if (status == 2) {
        content = '<span class="label label-default"> On Process</span>'
      } else if (status == 3) {
        content = '<span class="label label-default"> On Delivery</span>'
      } else if (status == 4) {
        content = '<span class="label label-success"> Lunas</span>'
      }
      $('#sale-status').html(content)
      $("#modal-sale-status").modal('hide');
    } 
  })
})
</script>
@endsection

@section('content')
<div class="row">
  <div class="col-md-4">
    <div class='widget white-bg friends-group clearfix'>
      <small class="text-muted">No. Invoice </small>
      <p>{{ $data['sale']->invoice_no }}</p> 
      <small class="text-muted">Tanggal Transaksi </small>
      <p>{{ $data['sale']->invoice_date->format('d-m-Y') }}</p> 
      <small class="text-muted">Nama Pelanggan</small>
      <p>{{ $data['sale']->customer ? $data['sale']->customer->name : '-' }}</p>
      <small class="text-muted">Status Pembayaran <a href="#" id="btn-edit-payment-status" class="float-right">Ubah</a></small>
      <p id="payment-status">
        @if($data['sale']->payment_status == 1)
          <span class="label label-warning"> Pending</span>
        @elseif($data['sale']->payment_status == 2)
          <span class="label label-success"> Lunas</span>
        @elseif($data['sale']->payment_status == 3)
          <span class="label label-danger"> Canceled</span>
        @endif
      </p>
      <small class="text-muted">Status Transaksi <a href="#" id="btn-edit-sale-status" class="float-right">Ubah</a></small>
      <p id="sale-status">
        @if($data['sale']->sale_status == 1)
          <span class="label label-warning"> Pending</span>
        @elseif($data['sale']->sale_status == 2)
          <span class="label label-default"> On Process</span>
        @elseif($data['sale']->sale_status == 3)
          <span class="label label-default"> On Delivery</span>
        @elseif($data['sale']->sale_status == 4)
          <span class="label label-success"> Selesai</span>
        @elseif($data['sale']->sale_status == 5)
          <span class="label label-danger"> Dibatalkan</span>
        @endif
      </p>
      @if($data['sale']->courier)
      <small class="text-muted">Kurir</small>
      <p>{{ strtoupper($data['sale']->courier)}} ({{$data['sale']->courier_services}})</p>
      <small class="text-muted">Alamat Pengiriman</small>
      <p style="line-height: 20px;">
        {{ $data['sale']->address_data['recipient_name'] }} <br>
        <small>{{ $data['sale']->address_data['address'] }} <br>
        {{ $data['sale']->address_data['subdistrict_name']}}, {{$data['sale']->address_data['city_name'] }}<br>
        {{ $data['sale']->address_data['province_name']}}, {{$data['sale']->address_data['postal_code'] }}<br>
        Telp./HP {{$data['sale']->address_data['phone']}}</small>
      </p>
      @endif
    </div>
  </div>
  <div class="col-md-8">
    @if($data['sale']->sale_status != 5 && $data['sale']->payment_status != 3)
    <div class="card">
      <div class="card-body">
        <div class="btn-group">
          <button type="button" class="btn text-primary" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i></button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @if($data['sale']->courier)
            <li class="dropdown-item"><a href="{{ url('sale/' . $data['sale']->id . '/waybill') }}"><i class="fa fa-ticket"></i> No. Resi</a></li>
            @endif
            {{-- <li class="dropdown-item"><a href="#"> Ubah Status</a></li> --}}
            <li class="dropdown-divider"></li>
            @if($data['sale']->courier)
            <li class="dropdown-item"><a href="{{ url('sale/' . $data['sale']->id . '/shippingLabel') }}" target="_blank"><i class="fa fa-envelope"></i> Label Pengiriman</a></li>
            @endif
            <li class="dropdown-item"><a href="#"><i class="fa fa-print"></i> Cetak Invoice</a></li>
            <li class="dropdown-divider"></li>
            <li class="dropdown-item"><a class="text-danger" id="cancel-btn" href="{{url('sale/'.$data['sale']->id.'/cancel')}}"><i class="fa fa-ban"></i> Batalkan Transaksi</a></li>
          </ul>
        </div>
        @if($data['sale']->courier)
        <a href="#" class="btn" id="history" >History Pengiriman</a>
        @endif
      </div>
    </div>
    @endif

    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-4 col-xs-6 b-r"> <small class="text-muted">Sub Total</small>
            <p>Rp {{ moneyFormatter($data['sale']->sub_total) }}</p>
          </div>
          <div class="col-md-4 col-xs-6 b-r"> <small class="text-muted">Biaya Kirim</small>
            <p>Rp {{ moneyFormatter($data['sale']->shipping_fee) }}</p>
          </div>
          <div class="col-md-4 col-xs-6 b-r"> <small class="text-muted">Total</small>
            <h2>Rp {{ moneyFormatter($data['sale']->total) }}</h2>
          </div>
        </div>
        <hr>
        <table class="table" id="itemsTable">
          <thead>
            <tr>
              <td class="text-center">Nama Produk</td>
              <td class="text-center">Jumlah</td>
              <td class="text-center">Harga Satuan</td>
              <td class="text-center">Total</td>
            </tr>
          </thead>
          <tbody>
            @foreach ($data['sale']->details as $d)
              <tr>
                <td>
                  <div>{{ $d->product->name }}</div>
                </td>
                <td>{{ $d->qty }} {{ $d->product->units }}</td>
                <td class="text-right">Rp {{ moneyFormatter($d->unit_price) }}</td>
                <td class="text-right">Rp {{ moneyFormatter($d->qty * $d->unit_price) }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade modal-history" tabindex="-1" role="dialog" aria-labelledby="myDefaultModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times"></span></button>
        <h5 class="modal-title" id="myDefaultModalLabel">History Pengiriman</h5>
      </div>
      <div class="modal-body">
        <table class="table" id="status-table">
          <thead>
            <tr>
              <td>Tanggal</td>
              <td>Status Pengiriman</td>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-payment-status" tabindex="-1" role="dialog" aria-labelledby="myDefaultModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times"></span></button>
        <h5 class="modal-title" id="myDefaultModalLabel">Status Pembayaran</h5>
      </div>
      <div class="modal-body">
        <form action="#" class="form-horizontal" id="form-payment-status">
          <div class="form-group">
            <label>Status Pembayaran<span style="color: #f4516c;">*</span></label>
            <select name="payment_status" id="payment_status" class="form-control">
            </select>
          </div>
          <div class="clearfix pull-right">
            <button type="button" class="btn  btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn  btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-sale-status" tabindex="-1" role="dialog" aria-labelledby="myDefaultModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-times"></span></button>
        <h5 class="modal-title" id="myDefaultModalLabel">Status Transaksi</h5>
      </div>
      <div class="modal-body">
        <form action="#" class="form-horizontal" id="form-sale-status">
          <div class="form-group">
            <label>Status Transaksi<span style="color: #f4516c;">*</span></label>
            <select name="sale_status" id="sale_status" class="form-control">
            </select>
          </div>
          <div class="clearfix pull-right">
            <button type="button" class="btn  btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn  btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection