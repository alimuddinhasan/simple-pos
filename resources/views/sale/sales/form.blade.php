@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('sale') !!}
@endsection

@section('styles')
<link href="{{ asset('assets/lib/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
<link href="{{ asset('select2/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('select2/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<style>
.select2-container{ width: 100% !important; }
.courier-tbl {
  border-collapse: collapse;
  width: 100%;
}

.courier-tbl tr {
  border-bottom: 1px solid #ccc;
  height: 50px;
}

.courier-tbl th {
  text-align: left;    
}

.courier-logo {
  min-width: 70px;
  max-width: 70px;
}
</style>
@endsection

@section('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="subdistrict-id" content="{{ Auth::user()->merchant->subdistrict_id }}">
<script type="text/javascript" src="{{ asset('assets/js/numeral.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/numeral.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lodash.js') }}"></script>
<script type="text/javascript" src="{{ asset('select2/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap-daterangepicker/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lodash.js') }}"></script>
<script type="text/javascript" src="{{ asset('custom_assets/sales/form.js') }}"></script>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-default">
        Tambah Penjualan
      </div>
      <div class="card-body">
        <form id="form" method="post" class="form-horizontal" action="#">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="invoice_date">Tanggal Transaksi<span style="color: #f4516c;">*</span></label>
                <div class="input-group m-b">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                  <input type="text" name="invoice_date" id="invoice_date"/>
                </div>
              </div>
              <div class="form-group">
                <label>Customer</label>
                <select name="customer_id" id="customer_id" class="form-control" style="width:100%;" ></select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="sale_status">Status Transaksi</label>
                <select name="sale_status" id="sale_status" class="form-control m-b">
                  <option value="4">Selesai</option>
                  <option value="2">Dalam Proses</option>
                  <option value="3">Dalam Pengiriman</option>
                  <option value="1">Belum Diproses</option>
                </select>
              </div>
              <div class="form-group">
                <label for="payment_status">Status Pembayaran</label>
                <div class="form-inline">
                  <div class="radio radio-primary">
                    <input type="radio" name="payment_status" value="2" checked> 
                    <label for="">Lunas</label>
                  </div>
                  <div class="radio radio-primary">
                    <input type="radio" name="payment_status" value="1"> 
                    <label for="">Belum Bayar</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <hr>
            </div>
            <div class="col-md-3">
              <h5>Shipping Fee</h5>
              <h3 id="shipping-fee">Rp 0</h3>
            </div>
            <div class="col-md-3">
              <h5>Sub Total</h5>
              <h3 id="sub-total">Rp 0</h3>
            </div>
            <div class="col-md-6">
              <h1 id="total" class="pull-right">Rp 0</h1>
              <h4>TOTAL</h4>
            </div>
            <div class="col-md-12">
              <div class="tabs">
                <ul class="nav nav-tabs">
                  <li class="nav-item" role="presentation"><a class="nav-link  active" href="#detail-barang" aria-controls="detail-barang" id="detail-barang-tab-btn" role="tab" data-toggle="tab">Detail Barang</a></li>
                </ul>
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="detail-barang">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <select name="product_id" id="product_id" class="form-control" style="width:100%;" ></select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <input type="number" class="form-control" id="qty" name="qty" placeholder="Jumlah" />
                        </div>
                      </div>
                      <div class="col-md-12">
                        <table class="table" id="itemsTable">
                          <thead>
                            <tr>
                              <td>Nama Produk</td>
                              <td>Harga Satuan (Rp)</td>
                              <td>Jumlah</td>
                              <td>Berat (gram)</td>
                              <td>Subtotal</td>
                              <td></td>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <hr>
                    <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>          
                    <div class="form-group">
                      <button id="next" class="btn btn-default pull-right">Next</button>
                      <a href="{{ url('sale') }}" class="btn btn-default">Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection