@extends('_layouts.base')

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $("#form").validate({
    rules: {
      name: "required",
      display_name: "required"
    },
    messages: {
      name: "Masukkan nama",
      display_name: "Masukkan display name"
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".form-group" ).addClass( "has-error" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).removeClass( "has-error" );
    }
  });
});
</script>
@endsection

@section('breadcrumbs')
{!! Breadcrumbs::render('role') !!}
@endsection

@section('content')
@php
  $action = url('role');
  $patch_field = '';
  if (isset($data['role'])) {
    $action = url('role/' . $data['role']->id);
    $patch_field = method_field('PATCH');
  }
@endphp
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header card-default">
        {{ isset($data['role']) ? 'Edit Role' : 'Create Role' }}
      </div>
      <div class="card-body">
        <form id="form" method="post" class="form-horizontal" action="{{ $action }}">
          {{ csrf_field() }}
          {{ $patch_field }}
          <div class="form-group">
            <label for="name">Name<span style="color: #f4516c;">*</span></label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" 
            @if(!empty($data['role']))
            value="{{ $data['role']->name }}" 
            @endif/>
          </div>
          <div class="form-group">
            <label for="display_name">Display Name<span style="color: #f4516c;">*</span></label>
            <input type="text" class="form-control" id="display_name" name="display_name" placeholder="Display Name" 
            @if(!empty($data['role']))
            value="{{ $data['role']->display_name }}"
            @endif/>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-12">
                <label>Permissions</label>
              </div>
            </div>
            <div class="row">
              <?php $i = 0; ?>
              @foreach($datalist['permissions'] as $d)
              @if($i === 0)
                @endif
                <div class="col-md-6">
                  <label>
                    <input type="checkbox" value="{{ $d->id }}" class="grey" name="permissions[]"
                    @if(!empty($data['role_permissions']))
                    @if($data['role_permissions']->contains($d->id))
                    checked 
                    @endif
                    @endif> 
                    {{ $d->display_name }}
                  </label>
                </div>
                <?php $i++; ?>
                @if($i === 2)
              <?php $i = 0; ?>
              @endif
              @endforeach
            </div>
          </div>
          <div class="checkbox checkbox-primary">
            <input id="active" name="active" type="checkbox" value="true"
              @if(!empty($data['role']))
              @if($data['role']->active)
              checked 
              @endif
              @else
              checked 
              @endif>
            <label for="active"> Active </label>
          </div>
          <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>
          <div class="form-group">
            <a href="{{ url('role') }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection