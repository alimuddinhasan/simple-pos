@extends('_layouts.base')

@section('breadcrumbs')
{!! Breadcrumbs::render('user') !!}
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/lib/jquery-validate/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready( function () {
  $("#form").validate({
    rules: {
      name: "required",
      email: {
        required: true,
        email: true,
        remote: "/user/check"
      },
      role: "required"
    },
    messages: {
      name: "Masukkan nama",
      email: {
        required: "Masukkan email",
        email: "Email tidak valid",
        remote: "Email sudah terdaftar"
      },
      role: "Pilih role user"
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
      error.addClass( "help-block" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.parent( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      $( element ).parents( ".form-group" ).addClass( "has-error" );
    },
    unhighlight: function (element, errorClass, validClass) {
      $( element ).parents( ".form-group" ).removeClass( "has-error" );
    }
  });
});
</script>
@endsection

@section('content')
@php
  $action = url('user');
  $patch_field = '';
  if (isset($data['user'])) {
    $action = url('user/' . $data['user']->id);
    $patch_field = method_field('PATCH');
  }
@endphp
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header card-default">
        {{ isset($data['user']) ? 'Edit User' : 'Create User' }}
      </div>
      <div class="card-body">
        <form id="form" method="post" class="form-horizontal" action="{{ $action }}">
          {{ csrf_field() }}
          {{ $patch_field }}
          <div class="form-group">
            <label for="name">Name<span style="color: #f4516c;">*</span></label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" 
            @if(!empty($data['user']))
            value="{{ $data['user']->name }}" 
            @endif/>
          </div>
          <div class="form-group">
            <label for="email">Email<span style="color: #f4516c;">*</span></label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Email" 
            @if(!empty($data['user']))
            value="{{ $data['user']->email }}" disabled
            @endif/>
          </div>
          <div class="form-group">
            <label>Role<span style="color: #f4516c;">*</span></label>
            <select name="role" id="role" class="form-control m-b">
              <option value="">-- Choose one --</option>
              @foreach($datalist['roles'] as $d)
              <option value="{{ $d->id }}"
                @if(!empty($data['user']))
                @if(isset($data['user']->roles[0]) ? $data['user']->roles[0]->id === $d->id : false)
                selected 
                @endif
                @endif>
                {{ $d->display_name }}
              </option>
              @endforeach
            </select>
          </div>
          <div class="checkbox checkbox-primary">
            <input id="active" name="active" type="checkbox" value="true"
              @if(!empty($data['user']))
              @if($data['user']->active)
              checked 
              @endif
              @else
              checked 
              @endif>
            <label for="active"> Active </label>
          </div>
          <p class="text-muted">Field bertanda <span style="color: #f4516c;">*</span> wajib diisi.</p>          
          <div class="form-group">
            <a href="{{ url('user') }}" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection