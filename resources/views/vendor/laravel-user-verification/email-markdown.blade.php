@component('mail::message')

Hai, Terima kasih telah mendaftar di BakulKasir.com!

@component('mail::button', ['url' => route('email-verification.check', $user->verification_token) . '?email=' . urlencode($user->email) ])
Klik Disini Untuk Verifikasi Akun Anda
@endcomponent

Terima kasih,<br>
{{ config('app.name') }}
@endcomponent
