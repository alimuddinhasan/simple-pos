<?php
Breadcrumbs::register('home', function($breadcrumbs) {
  $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('user', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('User', route('user'));
});

Breadcrumbs::register('role', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Role', route('role'));
});

Breadcrumbs::register('merchant', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Merchant', route('merchant'));
});

Breadcrumbs::register('merchantUser', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Merchant', route('merchant'));
  $breadcrumbs->push('Merchant User', route('merchantUser'));
});

Breadcrumbs::register('category', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Kategori', route('category'));
});

Breadcrumbs::register('product', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Produk', route('product'));
});

Breadcrumbs::register('supplier', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Supplier', route('supplier'));
});

Breadcrumbs::register('customer', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Pelanggan', route('customer'));
});

Breadcrumbs::register('purchase', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Pembelian', route('purchase'));
});

Breadcrumbs::register('inventoryIn', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Stok Masuk', route('inventoryIn'));
});

Breadcrumbs::register('inventoryOut', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Stok Keluar', route('inventoryOut'));
});

Breadcrumbs::register('stockOpname', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Stok Opname', route('stockOpname'));
});

Breadcrumbs::register('sale', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Penjualan', route('sale'));
});

Breadcrumbs::register('profile', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Profil', route('profile'));
});

Breadcrumbs::register('billing', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Tagihan', route('billing'));
});

Breadcrumbs::register('invoice', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Invoice', route('invoice'));
});

Breadcrumbs::register('profile.changepassword', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Profil', route('profile'));
  $breadcrumbs->push('Change Password', route('profile.changepassword'));
});

Breadcrumbs::register('profile.editprofile', function($breadcrumbs) {
  $breadcrumbs->parent('home');
  $breadcrumbs->push('Profil', route('profile'));
  $breadcrumbs->push('Ubah Profile', route('profile.editprofile'));
});