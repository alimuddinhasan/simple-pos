<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');
Route::get('user/check', ['uses' => 'Setting\UserController@checkEmail']);

Route::group(['middleware' => ['auth']], function () {
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/allSales', 'HomeController@allSales');

  ### --- USERS
  Route::group(['prefix' => 'user'], function () {
    Route::get('/', ['as' => 'user', 'uses' => 'Setting\UserController@index', 'middleware' => ['permission:read-users']]);
    Route::get('/fetch', ['as' => 'user.fetch', 'uses' => 'Setting\UserController@fetchAll', 'middleware' => ['permission:read-users']]);
    Route::get('/create', ['uses' => 'Setting\UserController@showForm', 'middleware' => ['permission:create-users']]);
    Route::post('/', ['uses' => 'Setting\UserController@store', 'middleware' => ['permission:create-users']]);
    Route::get('/{id}/edit', ['uses' => 'Setting\UserController@showForm', 'middleware' => ['permission:update-users']]);
    Route::patch('/{id}', ['uses' => 'Setting\UserController@update', 'middleware' => ['permission:update-users']]);
    Route::get('/{id}/delete', ['uses' => 'Setting\UserController@delete', 'middleware' => ['permission:delete-users']]);
    Route::get('/{id}/changepassword', ['uses' => 'Setting\UserController@showFormPassword', 'middleware' => ['permission:update-users']]);
    Route::post('/{id}/changepassword', ['uses' => 'Setting\UserController@updatePassword', 'middleware' => ['permission:update-users']]);
  });

  ### --- ROLES
  Route::group(['prefix' => 'role'], function () {
    Route::get('/', ['as' => 'role', 'uses' => 'Setting\RoleController@index', 'middleware' => ['permission:read-roles']]);
    Route::get('/fetch', ['as' => 'role.fetch', 'uses' => 'Setting\RoleController@fetchAll', 'middleware' => ['permission:read-roles']]);
    Route::post('/', ['uses' => 'Setting\RoleController@store', 'middleware' => ['permission:create-roles']]);
    Route::get('/create', ['uses' => 'Setting\RoleController@showForm', 'middleware' => ['permission:create-roles']]);
    Route::get('/{id}/edit', ['uses' => 'Setting\RoleController@showForm', 'middleware' => ['permission:update-roles']]);
    Route::patch('/{id}', ['uses' => 'Setting\RoleController@update', 'middleware' => ['permission:update-roles']]);
    Route::get('/{id}/delete', ['uses' => 'Setting\RoleController@delete', 'middleware' => ['permission:delete-roles']]);
  });

  ### --- MERCHANTS
  Route::group(['prefix' => 'merchant'], function () {
    Route::get('/', ['as' => 'merchant', 'uses' => 'Merchant\MerchantController@index', 'middleware' => ['permission:read-merchants']]);
    Route::get('/fetch', ['as' => 'merchant.fetch', 'uses' => 'Merchant\MerchantController@fetchAll', 'middleware' => ['permission:read-merchants']]);
    Route::get('/create', ['uses' => 'Merchant\MerchantController@showForm', 'middleware' => ['permission:create-merchants']]);
    Route::post('/', ['uses' => 'Merchant\MerchantController@store', 'middleware' => ['permission:create-merchants']]);
    Route::get('/{id}', ['uses' => 'Merchant\MerchantController@detail', 'middleware' => ['permission:read-merchants']]);
    Route::get('/{id}/edit', ['uses' => 'Merchant\MerchantController@showForm', 'middleware' => ['permission:update-merchants']]);
    Route::patch('/{id}', ['uses' => 'Merchant\MerchantController@update', 'middleware' => ['permission:update-merchants']]);
    Route::get('/{id}/delete', ['uses' => 'Merchant\MerchantController@delete', 'middleware' => ['permission:delete-merchants']]);
  });

  ### --- MERCHANTS USER
  Route::group(['prefix' => 'merchantUser'], function () {
    Route::get('/', ['as' => 'merchantUser', 'uses' => 'Merchant\UserController@index', 'middleware' => ['permission:read-merchant-users']]);
    Route::get('/fetch', ['as' => 'merchantUser.fetch', 'uses' => 'Merchant\UserController@fetchAll', 'middleware' => ['permission:read-merchant-users']]);
    Route::get('/create', ['uses' => 'Merchant\UserController@showForm', 'middleware' => ['permission:create-merchant-users']]);
    Route::post('/', ['uses' => 'Merchant\UserController@store', 'middleware' => ['permission:create-merchant-users']]);
    Route::get('/{id}/edit', ['uses' => 'Merchant\UserController@showForm', 'middleware' => ['permission:update-merchant-users']]);
    Route::patch('/{id}', ['uses' => 'Merchant\UserController@update', 'middleware' => ['permission:update-merchant-users']]);
    Route::get('/{id}/delete', ['uses' => 'Merchant\UserController@delete', 'middleware' => ['permission:delete-merchant-users']]);
    Route::get('/{id}/changepassword', ['uses' => 'Merchant\UserController@showFormPassword', 'middleware' => ['permission:update-merchant-users']]);
    Route::post('/{id}/changepassword', ['uses' => 'Merchant\UserController@updatePassword', 'middleware' => ['permission:update-merchant-users']]);
  });

  ### --- CATEGORY
  Route::group(['prefix' => 'category'], function () {
    Route::get('/', ['as' => 'category', 'uses' => 'Product\CategoryController@index', 'middleware' => ['permission:read-categories']]);
    Route::get('/fetch', ['as' => 'category.fetch', 'uses' => 'Product\CategoryController@fetchAll', 'middleware' => ['permission:read-categories']]);
    Route::get('/create', ['uses' => 'Product\CategoryController@showForm', 'middleware' => ['permission:create-categories']]);
    Route::post('/', ['uses' => 'Product\CategoryController@store', 'middleware' => ['permission:create-categories']]);
    Route::get('/{id}/edit', ['uses' => 'Product\CategoryController@showForm', 'middleware' => ['permission:update-categories']]);
    Route::patch('/{id}', ['uses' => 'Product\CategoryController@update', 'middleware' => ['permission:update-categories']]);
    Route::get('/{id}/delete', ['uses' => 'Product\CategoryController@delete', 'middleware' => ['permission:delete-categories']]);
  });

  ### --- PRODUCT
  Route::group(['prefix' => 'product'], function () {
    Route::get('/', ['as' => 'product', 'uses' => 'Product\ProductController@index', 'middleware' => ['permission:read-products']]);
    Route::get('/fetch', ['as' => 'product.fetch', 'uses' => 'Product\ProductController@fetchAll', 'middleware' => ['permission:read-products']]);
    Route::get('/find', ['as' => 'product.find', 'uses' => 'Product\ProductController@find', 'middleware' => ['permission:read-products|create-purchases']]);
    Route::get('/create', ['uses' => 'Product\ProductController@showForm', 'middleware' => ['permission:create-products']]);
    Route::post('/', ['uses' => 'Product\ProductController@store', 'middleware' => ['permission:create-products']]);
    Route::get('/{id}', ['uses' => 'Product\ProductController@get', 'middleware' => ['permission:read-products|create-purchases']]);
    Route::get('/{id}/edit', ['uses' => 'Product\ProductController@showForm', 'middleware' => ['permission:update-products']]);
    Route::patch('/{id}', ['uses' => 'Product\ProductController@update', 'middleware' => ['permission:update-products']]);
    Route::get('/{id}/delete', ['uses' => 'Product\ProductController@delete', 'middleware' => ['permission:delete-products']]);
  });

  ### --- SUPPLIER
  Route::group(['prefix' => 'supplier'], function () {
    Route::get('/', ['as' => 'supplier', 'uses' => 'Inventory\SupplierController@index', 'middleware' => ['permission:read-suppliers']]);
    Route::get('/fetch', ['as' => 'supplier.fetch', 'uses' => 'Inventory\SupplierController@fetchAll', 'middleware' => ['permission:read-suppliers']]);
    Route::get('/create', ['uses' => 'Inventory\SupplierController@showForm', 'middleware' => ['permission:create-suppliers']]);
    Route::post('/', ['uses' => 'Inventory\SupplierController@store', 'middleware' => ['permission:create-suppliers']]);
    Route::get('/{id}/edit', ['uses' => 'Inventory\SupplierController@showForm', 'middleware' => ['permission:update-suppliers']]);
    Route::patch('/{id}', ['uses' => 'Inventory\SupplierController@update', 'middleware' => ['permission:update-suppliers']]);
    Route::get('/{id}/delete', ['uses' => 'Inventory\SupplierController@delete', 'middleware' => ['permission:delete-suppliers']]);
  });

  ### --- PURCHASE
  Route::group(['prefix' => 'purchase'], function () {
    Route::get('/', ['as' => 'purchase', 'uses' => 'Inventory\PurchaseController@index', 'middleware' => ['permission:read-purchases']]);
    Route::get('/fetch', ['as' => 'purchase.fetch', 'uses' => 'Inventory\PurchaseController@fetchAll', 'middleware' => ['permission:read-purchases']]);
    Route::get('/create', ['uses' => 'Inventory\PurchaseController@showForm', 'middleware' => ['permission:create-purchases']]);
    Route::post('/', ['uses' => 'Inventory\PurchaseController@store', 'middleware' => ['permission:create-purchases']]);
    Route::get('/{id}', ['uses' => 'Inventory\PurchaseController@detail', 'middleware' => ['permission:read-purchases']]);
    Route::get('/{id}/edit', ['uses' => 'Inventory\PurchaseController@showForm', 'middleware' => ['permission:update-purchases']]);
    Route::patch('/{id}', ['uses' => 'Inventory\PurchaseController@update', 'middleware' => ['permission:update-purchases']]);
    Route::get('/{id}/delete', ['uses' => 'Inventory\PurchaseController@delete', 'middleware' => ['permission:delete-purchases']]);
    Route::get('/{id}/receive', ['uses' => 'Inventory\PurchaseController@showReceiveForm', 'middleware' => ['permission:receive-purchases']]);
    Route::patch('/{id}/receive', ['uses' => 'Inventory\PurchaseController@receiveUpdate', 'middleware' => ['permission:receive-purchases']]);
  });

  ### --- INVENTORY IN
  Route::group(['prefix' => 'inventoryIn'], function () {
    Route::get('/', ['as' => 'inventoryIn', 'uses' => 'Inventory\InventoryInController@index', 'middleware' => ['permission:read-inventories']]);
    Route::get('/fetch', ['as' => 'inventoryIn.fetch', 'uses' => 'Inventory\InventoryInController@fetchAll', 'middleware' => ['permission:read-inventories']]);
    Route::get('/create', ['uses' => 'Inventory\InventoryInController@showForm', 'middleware' => ['permission:create-inventories']]);
    Route::post('/', ['uses' => 'Inventory\InventoryInController@store', 'middleware' => ['permission:create-inventories']]);
    Route::get('/{id}', ['uses' => 'Inventory\InventoryInController@detail', 'middleware' => ['permission:read-inventories']]);
  });

  ### --- INVENTORY OUT
  Route::group(['prefix' => 'inventoryOut'], function () {
    Route::get('/', ['as' => 'inventoryOut', 'uses' => 'Inventory\InventoryOutController@index', 'middleware' => ['permission:read-inventories']]);
    Route::get('/fetch', ['as' => 'inventoryOut.fetch', 'uses' => 'Inventory\InventoryOutController@fetchAll', 'middleware' => ['permission:read-inventories']]);
    Route::get('/create', ['uses' => 'Inventory\InventoryOutController@showForm', 'middleware' => ['permission:create-inventories']]);
    Route::post('/', ['uses' => 'Inventory\InventoryOutController@store', 'middleware' => ['permission:create-inventories']]);
    Route::get('/{id}', ['uses' => 'Inventory\InventoryOutController@detail', 'middleware' => ['permission:read-inventories']]);
  });

  ### --- STOCK OPNAME
  Route::group(['prefix' => 'stockOpname'], function () {
    Route::get('/', ['as' => 'stockOpname', 'uses' => 'Inventory\StockOpnameController@index', 'middleware' => ['permission:read-inventories']]);
    Route::get('/fetch', ['as' => 'stockOpname.fetch', 'uses' => 'Inventory\StockOpnameController@fetchAll', 'middleware' => ['permission:read-inventories']]);
    Route::get('/create', ['uses' => 'Inventory\StockOpnameController@showForm', 'middleware' => ['permission:create-inventories']]);
    Route::post('/', ['uses' => 'Inventory\StockOpnameController@store', 'middleware' => ['permission:create-inventories']]);
    Route::get('/{id}', ['uses' => 'Inventory\StockOpnameController@detail', 'middleware' => ['permission:read-inventories']]);
  });

  ### --- CUSTOMER
  Route::group(['prefix' => 'customer'], function () {
    Route::get('/', ['as' => 'customer', 'uses' => 'Customer\CustomerController@index', 'middleware' => ['permission:read-sales']]);
    Route::get('/fetch', ['as' => 'customer.fetch', 'uses' => 'Customer\CustomerController@fetchAll', 'middleware' => ['permission:read-customers']]);
    Route::get('/find', ['as' => 'customer.find', 'uses' => 'Customer\CustomerController@find', 'middleware' => ['permission:read-customers|create-sales']]);
    Route::get('/create', ['uses' => 'Customer\CustomerController@showForm', 'middleware' => ['permission:create-customers']]);
    Route::post('/', ['uses' => 'Customer\CustomerController@store', 'middleware' => ['permission:create-customers']]);
    Route::get('/{id}', ['uses' => 'Customer\CustomerController@get', 'middleware' => ['permission:read-customers|create-sales']]);
    Route::get('/{id}/detail', ['uses' => 'Customer\CustomerController@detail', 'middleware' => ['permission:read-customers']]);
    Route::get('/{id}/edit', ['uses' => 'Customer\CustomerController@showForm', 'middleware' => ['permission:update-customers']]);
    Route::patch('/{id}', ['uses' => 'Customer\CustomerController@update', 'middleware' => ['permission:update-customers']]);
    Route::get('/{id}/delete', ['uses' => 'Customer\CustomerController@delete', 'middleware' => ['permission:delete-customers']]);
  });

  ### --- PROFILE
  Route::group(['prefix' => 'profile'], function () {
    Route::get('/', ['as' => 'profile', 'uses' => 'Profile\ProfileController@index', 'middleware' => ['permission:read-user-profile|read-bussiness-profile']]);
    Route::get('/changepassword', ['as' => 'profile.changepassword', 'uses' => 'Profile\ProfileController@showChangePasswordForm', 'middleware' => ['permission:update-user-profile']]);
    Route::post('/changepassword', ['uses' => 'Profile\ProfileController@changePassword', 'middleware' => ['permission:update-user-profile']]);
    Route::get('/editprofile', ['as' => 'profile.editprofile', 'uses' => 'Profile\ProfileController@showEditProfileForm', 'middleware' => ['permission:update-user-profile']]);
    Route::post('/editprofile', ['uses' => 'Profile\ProfileController@editProfile', 'middleware' => ['permission:update-user-profile']]);
    Route::get('/editbussinessprofile', ['as' => 'profile.editbussinessprofile', 'uses' => 'Profile\ProfileController@showEditBussinessProfileForm', 'middleware' => ['permission:update-bussiness-profile']]);
    Route::post('/editbussinessprofile', ['uses' => 'Profile\ProfileController@editBussinessProfile', 'middleware' => ['permission:update-bussiness-profile']]);
  });

  ### --- BILLING
  Route::group(['prefix' => 'billing'], function () {
    Route::get('/', ['as' => 'billing', 'uses' => 'Billing\BillingController@index', 'middleware' => ['permission:read-billing']]);
    Route::get('/request', ['uses' => 'Billing\BillingController@showRequestForm', 'middleware' => ['permission:read-billing']]);
    Route::post('/request', ['uses' => 'Billing\BillingController@request', 'middleware' => ['permission:read-billing']]);
  });

  ### --- INVOICE
  Route::group(['prefix' => 'invoice'], function () {
    Route::get('/', ['as' => 'invoice', 'uses' => 'Invoice\InvoiceController@index', 'middleware' => ['permission:read-invoice']]);
    Route::get('/fetch', ['as' => 'invoice.fetch', 'uses' => 'Invoice\InvoiceController@fetchAll', 'middleware' => ['permission:read-invoice']]);
    Route::get('/{id}', ['uses' => 'Invoice\InvoiceController@detail', 'middleware' => ['permission:read-invoice']]);
  });

  ### --- PLAN
  Route::group(['prefix' => 'plan'], function () {
    Route::get('/{id}/get', ['uses' => 'Plan\PlanController@get', 'middleware' => ['permission:read-billing']]);
  });

  ### --- SALE
  Route::group(['prefix' => 'sale'], function () {
    Route::get('/', ['as' => 'sale', 'uses' => 'Sale\SaleController@index', 'middleware' => ['permission:read-sales']]);
    Route::get('/fetch', ['as' => 'sale.fetch', 'uses' => 'Sale\SaleController@fetchAll', 'middleware' => ['permission:read-sales']]);
    Route::get('/create', ['uses' => 'Sale\SaleController@showForm', 'middleware' => ['permission:create-sales']]);
    Route::post('/', ['uses' => 'Sale\SaleController@store', 'middleware' => ['permission:create-sales']]);
    Route::get('/{id}', ['uses' => 'Sale\SaleController@detail', 'middleware' => ['permission:read-sales']]);
    Route::get('/{id}/get', ['uses' => 'Sale\SaleController@get', 'middleware' => ['permission:read-sales|read-customers']]);
    Route::get('/{id}/edit', ['uses' => 'Sale\SaleController@showForm', 'middleware' => ['permission:update-sales']]);
    Route::patch('/{id}', ['uses' => 'Sale\SaleController@update', 'middleware' => ['permission:update-sales']]);
    Route::get('/{id}/cancel', ['uses' => 'Sale\SaleController@cancelTransaction', 'middleware' => ['permission:delete-sales']]);
    Route::get('/{id}/waybill', ['uses' => 'Sale\SaleController@showWaybillForm', 'middleware' => ['permission:update-sales']]);
    Route::patch('/{id}/waybill', ['uses' => 'Sale\SaleController@updateWaybill', 'middleware' => ['permission:update-sales']]);
    Route::get('/{id}/shippingLabel', ['uses' => 'Sale\SaleController@showShippingLabel', 'middleware' => ['permission:read-sales']]);
    Route::patch('/{id}/updateStatus', ['uses' => 'Sale\SaleController@updateStatus', 'middleware' => ['permission:update-sales']]);
  });

  ### --- RAJA ONGKIR
  Route::group(['prefix' => 'courier'], function () {
    Route::get('/province', ['as' => 'rajaongkir.province', 'uses' => 'RajaOngkir\RajaOngkirController@findProvince', 'middleware' => ['permission:create-sales|create-customers|create-merchants']]);
    Route::get('/city', ['as' => 'rajaongkir.city', 'uses' => 'RajaOngkir\RajaOngkirController@findCity', 'middleware' => ['permission:create-sales|create-customers|create-merchants']]);
    Route::get('/subdistrict', ['as' => 'rajaongkir.subdistrict', 'uses' => 'RajaOngkir\RajaOngkirController@findSubdistrict', 'middleware' => ['permission:create-sales|create-customers|create-merchants']]);
    Route::post('/cost', ['as' => 'rajaongkir.cost', 'uses' => 'RajaOngkir\RajaOngkirController@getCost', 'middleware' => ['permission:create-sales|create-customers|create-merchants']]);
    Route::post('/waybill', ['as' => 'rajaongkir.waybill', 'uses' => 'RajaOngkir\RajaOngkirController@getWaybill', 'middleware' => ['permission:read-purchases']]);
  });

});
